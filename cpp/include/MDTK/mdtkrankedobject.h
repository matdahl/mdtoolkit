/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "mdtkobject.h"

//! A basic object that has an integer rank property by which it is sortable
class MDTKRankedObject : public MDTKObject
{
    Q_OBJECT
    Q_PROPERTY(int rank READ rank WRITE setRank NOTIFY rankChanged FINAL)

public:
    MDTKRankedObject(QObject *parent = nullptr, uint uid = 0, int rank = 0)
        : MDTKObject(parent,uid), m_rank(rank) {}

    /* --- QML properties - getter --- */
    int rank() const { return m_rank; }

    /* --- QML properties - setter --- */
    void setRank(const int rank)
    {
        if (m_rank == rank)
            return;

        m_rank = rank;
        emit rankChanged(this);
        emit rankingChanged(this);
    }

    virtual bool isLessThan(const MDTKObject *other) const override
    {
        const MDTKRankedObject *castedOther = qobject_cast<const MDTKRankedObject*>(other);
        return castedOther && this->rank() < castedOther->rank();
    }

signals:
    void rankChanged(MDTKRankedObject *obj);

private:
    int m_rank;
};

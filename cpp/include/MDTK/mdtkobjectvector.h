/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "mdtkobject.h"

#include <QVector>

template <typename T = MDTKObject>
class MDTKObjectVector : public QVector<T*>
{
public:
    int find(const uint uid) const
    {
        const int N = this->size();
        for (int i=0; i<N; i++)
            if (this->at(i)->uid() == uid)
                return i;
        return -1;
    }

    T *get(const uint uid) const
    {
        const int idx = this->find(uid);
        return idx >= 0 ? this->at(idx) : nullptr;
    }
};

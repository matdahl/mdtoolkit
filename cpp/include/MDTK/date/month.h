/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * finance is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QDate>
#include <QDateTime>
#include <QDebug>
#include <QString>

class Month
{
public:
    inline Month(const QString &datestr, const QString &format = "yyyy/MM") : Month(QDate::fromString(datestr,format)) {}
    Month(const QDate &date);
    Month(const int year, const int month = 1, const bool isValid = true);
    Month();

    inline int  month()   const {return m_month;}
    inline int  year()    const {return m_year;}
    inline bool isValid() const {return m_isValid;}

    Month addMonths(const int m) const;
    Month addYears(const int y) const;

    QDate toDate(const int day = 15) const;
    QDateTime toDateTime(const int day = 15) const;
    QString toString(const QString &format) const;
    operator QString() const;
    int daysInMonth() const {return QDate(m_year,m_month,1).daysInMonth();}

    static const QDate futureInfinity;
    static Month currentMonth() {
        return Month(QDate::currentDate());
    }

private:
    int  m_month = 0;
    int  m_year  = 0;
    bool m_isValid = false;
};

bool operator==(const Month &a, const Month &b);
bool operator==(const Month &a, const QDate       &b);
bool operator==(const QDate       &a, const Month &b);

bool operator!=(const Month &a, const Month &b);
bool operator!=(const Month &a, const QDate       &b);
bool operator!=(const QDate       &a, const Month &b);

bool operator>(const Month &a, const Month &b);
bool operator>(const Month &a, const QDate       &b);
bool operator>(const QDate       &a, const Month &b);

bool operator<(const Month &a, const Month &b);
bool operator<(const Month &a, const QDate       &b);
bool operator<(const QDate       &a, const Month &b);

bool operator>=(const Month &a, const Month &b);
bool operator>=(const Month &a, const QDate       &b);
bool operator>=(const QDate       &a, const Month &b);

bool operator<=(const Month &a, const Month &b);
bool operator<=(const Month &a, const QDate &b);
bool operator<=(const QDate &a, const Month &b);

int operator-(const Month &a, const Month &b);
Month operator+(const Month &a, const int &b);
Month operator-(const Month &a, const int &b);

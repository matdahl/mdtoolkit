/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QDebug>
#include <QString>
#include <QMap>
#include <QList>
#include <QDate>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QStandardPaths>
#include <QFile>

#include <MDTK/date/month.h>

namespace MDTK {

class AbstractDatabaseClient
{
public:
    enum ColumnType{
        AUTOINCREMENT,
        TEXT,
        INTEGER,
        REAL,
        DATE,
        MONTH,
        BOOLEAN
    };

    struct TableColumn {
        TableColumn(const QString &n = "", const ColumnType t = ColumnType::TEXT, const QString dv = "")
            : name(n), type(t), defaultValue(dv) {}

        QString    name;
        ColumnType type = TEXT;
        QString    defaultValue;
    };
    typedef QList<TableColumn> TableSchema;
    typedef QList<TableSchema> TableSchemaHistory;
    typedef QMap<QString,TableSchemaHistory> TableMap;

    struct TableMigrationEvent{
        QString tableName;
        int     fromScheme;
        int     toScheme;
        bool    handled = false;
        QSqlDatabase *db;
    };

    // returns the name of the database connection - must be implemented by child classes
    virtual QString name() const = 0;

    // returns a list of table schemas, including deprecated schemas, that the database should contain
    virtual TableMap tables() const = 0;

    // defines the path of the database file
    virtual QString filepath() const;

protected:
    // initialises the database - must be run in the constructor of all child classes
    void init();

    // opens a new database connection
    QSqlDatabase openDatabase();

    // creates a table with given name and schema in the opened database
    bool createTable(const QString &tableName, const TableSchema &schema, QSqlDatabase &db);

    // executes a SELECT * FROM tableName and returns the result
    QSqlQuery selectAll(const QString &tableName, QSqlDatabase &db, const QString &sortColumn = "", const bool ascending = true);

    // inserts all entries of values in given table and returns the last insert index. If not succesfull, it returns -1
    int simpleInsert(const QString &tableName, const QMap<QString,QString> values);

    // updates all entries of values in given table where the column "keyColumnName" has the value "key"
    bool simpleUpdate(const QString &tableName, const QMap<QString,QString> values, const QString &key, const QString &keyColumnName = QString("uid"));

    // removes all entries from given table where the column "keyColumnName" has the value "key"
    bool simpleDelete(const QString &tableName, const QString &key, const QString &keyColumnName = QString("uid"));

    // removes all entries from given table where all {columnName,value} conditions match
    bool multiDelete(const QString &tableName, const QMap<QString,QString> &conditions);

    // defines eventuall migrations from one table version to an other.
    // if this function does not set event.handled = true, then the program will exit.
    virtual void handleMigrations(TableMigrationEvent &event) {}

    // evaluates the result of a query and output error message if needed
    bool evaluateQuery(QSqlQuery &q, const QString &context = "");

    // checks whether the table exists
    bool hasTable(const QString &tableName, QSqlDatabase &db);

    // checks whether the table has a column with given name
    bool tableHasColumn(const QString &tableName, const QString columnName, QSqlDatabase &db);

    // log outputs
    void printDebug  (const QString &message) const;
    void printInfo   (const QString &message) const;
    void printWarning(const QString &message) const;
    void printError  (const QString &message) const;

    // parse query data
    QString parseText(QSqlQuery &q, const QString &colName) const;
    int     parseInteger(QSqlQuery &q, const QString &colName) const;
    uint    parseUInt(QSqlQuery &q, const QString &colName) const;
    qreal   parseReal(QSqlQuery &q, const QString &colName) const;
    QDate   parseDate(QSqlQuery &q, const QString &colName) const;
    QDate   parseMonth(QSqlQuery &q, const QString &colName) const;
    bool    parseBoolean(QSqlQuery &q, const QString &colName) const;

    // convert parameters to SQL string
    QString writeText   (const QString &value) const;
    QString writeInteger(const int      value) const;
    QString writeUInt   (const uint     value) const;
    QString writeReal   (const qreal    value) const;
    QString writeDate   (const QDate   &value) const;
    QString writeMonth  (const Month   &value) const;
    QString writeBoolean(const bool     value) const;

private:
    // matches the given list of column names with a scheme version of the given table - if it couldn't match it returns -1
    int matchScheme(const QString &tableName, const QStringList &columns);

    // handles the migration process when a deprecated table scheme was detected.
    void requestMigration(const QString &tableName, const int fromScheme, const int toScheme, QSqlDatabase &db);

};

} // namespace MDTK

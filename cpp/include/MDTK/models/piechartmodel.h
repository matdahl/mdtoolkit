/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * finance is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QAbstractListModel>
#include <QColor>
#include <QVector>

struct PieChartModelNode{
    PieChartModelNode(const uint uid = 0, const QColor &color = QColor(), const qreal value = 0.)
        : uid(uid), color(color), value(value) {}
    uint   uid;
    QColor color;
    qreal  value;
    qreal  priorValues = 0.;
    uint   count = 1;
};

class PieChartModelNodeList : public QVector<PieChartModelNode>
{
public:
    int find (const uint uid) const {
        for (int i=0; i<size(); i++)
            if (at(i).uid == uid)
                return i;
        return -1;
    }
};



class PieChartModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(uint  count READ count NOTIFY countChanged)
    Q_PROPERTY(qreal sum READ sum NOTIFY sumChanged)

public:
    enum Roles {
        UIDROLE = Qt::UserRole,
        COLORROLE,
        VALUEROLE,
        PRIORVALUESROLE
    };

    explicit PieChartModel(QObject *parent = nullptr)
        : QAbstractListModel(parent){}

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    inline uint  count() const {return m_list.size();}
    inline qreal sum()   const {return m_list.empty() ? 0 : m_list.back().priorValues + m_list.back().value;}

    // QML interface
    Q_INVOKABLE qreal getValue(const uint uid) const;

    // C++ interface
    void addValue(const uint uid, const qreal value, const QColor &color);
    void removeValue(const uint uid, const qreal value);
    void updateColor(const uint uid, const QColor &color);

signals:
    void countChanged();
    void sumChanged();

private:
    PieChartModelNodeList m_list;

    void updatePriors();
};

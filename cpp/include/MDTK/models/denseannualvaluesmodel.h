/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QAbstractListModel>
#include <QVector>
#include <QDate>

class DenseAnnualValuesModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(uint  count     READ count     NOTIFY countChanged)
    Q_PROPERTY(qreal valuesMin READ valuesMin NOTIFY valuesMinChanged)
    Q_PROPERTY(qreal valuesMax READ valuesMax NOTIFY valuesMaxChanged)
    Q_PROPERTY(int   firstYear READ firstYear NOTIFY firstYearChanged)
    Q_PROPERTY(int   lastYear  READ lastYear  NOTIFY lastYearChanged)

public:
    enum Roles{
        YEARROLE = Qt::UserRole,
        COUNTSROLE,
        VALUESROLE
    };

    struct Node{
        Node(int numRoles = 1, const int y = 0) : year(y){
            values.insert(0,numRoles,0.);
        }

        int            year;
        QVector<qreal> values;
        int            counts = 0;
    };

    explicit DenseAnnualValuesModel(QObject *parent = nullptr, const QVector<QString> roles = {}, const int firstYear = QDate::currentDate().year(), const int lastYear = QDate::currentDate().year());

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    // QML properties - getter
    uint  count() const {return m_list.size();}
    int   firstYear() const {return m_firstYear;}
    int   lastYear()  const {return m_lastYear;}
    qreal valuesMin() const {return m_valuesMin;}
    qreal valuesMax() const {return m_valuesMax;}

    // C++ interface
    const QVector<Node> &list() const {return m_list;}

    void setFirstYear(const int y, const bool extendOnly = true);
    void setLastYear(const int y, const bool extendOnly = true);
    void addValues(const int y, const QVector<qreal> &values);
    void removeValues(const int y, const QVector<qreal> &values);

signals:
    void countChanged();
    void valuesMinChanged();
    void valuesMaxChanged();
    void firstYearChanged();
    void lastYearChanged();

private:
    const QVector<QString> m_roles;
    const int              m_numRoles;
    QVector<Node> m_list;

    int   m_firstYear = 0;
    int   m_lastYear  = 0;
    qreal m_valuesMin = 0.;
    qreal m_valuesMax = 0.;

    void updateExtremeValues();
};


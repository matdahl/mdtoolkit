/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QAbstractListModel>
#include <QVector>
#include <QColor>

class MDTKColorModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)

public:
    enum Roles {
        COLORROLE = Qt::UserRole,
        COUNTSROLE
    };

    struct Node {
        Node(const QColor &_color = QColor(), const int _counts = 0)
            : color(_color), counts(_counts) {}

        QColor color;
        int    counts;
    };

    MDTKColorModel(QObject *parent = nullptr) : QAbstractListModel(parent) {}

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    // QML properties - getter
    int count() const {return m_list.size();}

    // QML interface
    Q_INVOKABLE QColor colorAt(const int idx) const;

    // C++ interface
    void insertColor(const int idx, const QColor &color);
    void replaceColor(const int idx, const QColor &color);
    void removeColor(const int idx);
    void appendColor(const QColor &color);
    void clear();
    void resize(const int count);

    void incCounter(const int idx);
    void decCounter(const int idx);

signals:
    void countChanged();

private:
    QVector<Node> m_list;
};

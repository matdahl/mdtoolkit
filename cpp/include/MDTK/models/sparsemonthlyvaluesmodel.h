/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QAbstractListModel>
#include <QVector>

#include <MDTK/date/month.h>

class SparseMonthlyValuesModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(uint      count      READ count          NOTIFY countChanged)
    Q_PROPERTY(qreal     valuesMin  READ valuesMin      NOTIFY valuesMinChanged)
    Q_PROPERTY(qreal     valuesMax  READ valuesMax      NOTIFY valuesMaxChanged)
    Q_PROPERTY(QDateTime firstMonth READ firstMonthTime NOTIFY firstMonthChanged)
    Q_PROPERTY(QDateTime lastMonth  READ lastMonthTime  NOTIFY lastMonthChanged)

public:
    enum Roles{
        MONTHROLE = Qt::UserRole,
        COUNTSROLE,
        VALUESROLE
    };

    struct Node{
        Node(int numRoles = 1, const Month &m = Month()) : month(m){
            values.insert(0,numRoles,0.);
        }

        Month          month;
        QVector<qreal> values;
        int            counts   = 0;
    };

    explicit SparseMonthlyValuesModel(QObject *parent = nullptr, const QVector<QString> &roles = {})
        : QAbstractListModel(parent), m_roles(roles), m_numRoles(m_roles.size()) {}

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    // QML properties - getter
    uint  count() const {return m_list.size();}
    QDateTime firstMonthTime() const {return QDateTime(firstMonth().toDate());}
    QDateTime lastMonthTime()  const {return QDateTime(lastMonth().toDate());}
    qreal valuesMin() const {return m_valuesMin;}
    qreal valuesMax() const {return m_valuesMax;}

    // QML interface
    Q_INVOKABLE bool hasMonth(const QDateTime &month) const;

    // C++ interface
    const QVector<Node> &list() const {return m_list;}
    Month firstMonth() const {return m_list.size()>0 ? m_list.front().month : Month();}
    Month lastMonth()  const {return m_list.size()>0 ? m_list.back().month  : Month();}

    void addValues(const Month &m, const QVector<qreal> &values);
    void removeValues(const Month &m, const QVector<qreal> &values);

signals:
    void countChanged();
    void valuesMinChanged();
    void valuesMaxChanged();
    void firstMonthChanged();
    void lastMonthChanged();

private:
    const QVector<QString> m_roles;
    const int              m_numRoles;
    QVector<Node> m_list;

    qreal m_valuesMin = 0.;
    qreal m_valuesMax = 0.;

    int getIndex(const Month &month, bool insertIfNotExists = true);
    void updateExtremeValues();
};


/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QAbstractListModel>
#include <QMap>
#include <QDate>

namespace MDTK {

class SparseDailyCountsModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(int sum   READ sum   NOTIFY sumChanged)

public:
    enum Roles {
        DATEROLE = Qt::UserRole,
        COUNTSROLE
    };

    SparseDailyCountsModel(QObject *parent = nullptr) : QAbstractListModel(parent) {}

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    // QML properties - getter
    int count() const {return m_map.size();}
    int sum()   const {return m_sum;}

    // C++ interface
    int getCounts(const QDate &date) const;

    void addCounts(const QDate &date, const int value);

signals:
    void countChanged();
    void sumChanged();

private:
    QMap<QDate,int> m_map;
    int m_sum = 0;
};

} // namespace MDTK

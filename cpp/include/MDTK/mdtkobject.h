/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QObject>

//! A basic object that has an integer uid by which it is sortable
class MDTKObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(uint uid READ uid CONSTANT)

public:
    MDTKObject(QObject *parent = nullptr, const uint uid = 0)
        : QObject(parent), m_uid(uid){}
    ~MDTKObject() { emit destruction(this); }

    uint uid() const { return m_uid; }

    void setUid(const uint uid)
    {
        m_uid = uid;
        emit uidChanged(this);
        emit rankingChanged(this);
    }

    //! Function to make Objects sortable.
    //! To be overridden if a derived class should be sorted by something other than the uid
    virtual bool isLessThan(const MDTKObject *other) const { return this->uid() < other->uid(); }

    bool operator<(const MDTKObject &other)  const { return this->isLessThan(&other); }
    bool operator==(const MDTKObject &other) const { return m_uid == other.uid(); }
    bool operator!=(const MDTKObject &other) const { return m_uid != other.uid(); }

signals:
    void uidChanged(MDTKObject *obj);

    // to be emitted when the sort ranking of the object has changed
    void rankingChanged(MDTKObject *obj);

    void destruction(MDTKObject *obj);

private:
    uint m_uid;
};

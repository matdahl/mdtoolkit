/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QAbstractListModel>

#include <QDebug>
#include <QVector>

#include "mdtkobject.h"
#include "mdtkobjectvector.h"


class MDTKObjectModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(uint count READ count NOTIFY countChanged FINAL)
    Q_PROPERTY(int  sortMode READ sortMode WRITE setSortMode NOTIFY sortModeChanged FINAL)

public:
    enum SortMode {
        UNSORTED,
        SORT_ASCENDING,
        SORT_DESCENDING
    };
    Q_ENUMS(SortMode)

    enum Roles {
        OBJECTROLE = Qt::UserRole
    };

    MDTKObjectModel(const QString &objName = QString(), QObject *parent = nullptr, SortMode sortMode = UNSORTED)
        : QAbstractListModel(parent), m_sortMode(sortMode)
    {
        this->setObjectName(objName);
    }

    /* --- QML properties - getter --- */
    uint count()    const {return m_list.size();}
    int  sortMode() const {return m_sortMode;}

    /* --- QML properties - setter --- */
    void setSortMode(const int sortMode);

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &) const { return m_list.size(); }
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    // other QML interface
    Q_INVOKABLE int indexOf(const uint uid) const {return m_list.find(uid);}
    Q_INVOKABLE MDTKObject *get(const uint idx) const {return (idx < m_list.size()) ? m_list.at(idx) : nullptr;}

    Q_INVOKABLE void clear();
    Q_INVOKABLE void insert(MDTKObject *obj) { doInsertAt(getInsertIdx(obj),obj); }
    Q_INVOKABLE void insertAt(MDTKObject *obj, const int idx);
    Q_INVOKABLE bool remove(MDTKObject *obj) { return doRemove(obj);}
    Q_INVOKABLE bool removeByUid(const uint uid);

    //! virtual function that defines the QML role name for the object stored in this model
    virtual QString objectRoleName() const { return "object"; }

signals:
    void countChanged();
    void sortModeChanged();

private:
    MDTKObjectVector<> m_list;
    SortMode           m_sortMode;

    //! Returns the index where obj should be inserted.
    int getInsertIdx(const MDTKObject *obj) const;

    //! Resorts the entire model
    void resort();

    //! inserts an element at given position. The idx must be in range and comply to current sort mode
    void doInsertAt(const int idx, MDTKObject *obj);

private slots:
    //! Resorts the model element == obj
    void resort(MDTKObject *obj);

    //! removes an element. If the model does not contain this element, nothing happens
    bool doRemove(MDTKObject *obj);
};

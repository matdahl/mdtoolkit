/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QDebug>
#include <QDir>
#include <QStandardPaths>
#include <QString>

void startupPrompt(const QString &appTitle)
{
    const QString prefix   = "#### Starting   ";
    const QString suffix   = "   ####";
    const QString fillLine = QString().fill('#',prefix.size()+appTitle.size()+suffix.size());

    qInfo().noquote().nospace() << fillLine;
    qInfo().noquote().nospace() << prefix << appTitle << suffix;
    qInfo().noquote().nospace() << fillLine;
}

void setMessagePattern()
{
    qSetMessagePattern("[%{time hh:mm:ss.zzz}] [%{if-debug}Debug]%{endif}%{if-info}Info] %{endif}%{if-warning}Warn] %{endif}%{if-critical}Crit] %{endif}%{if-fatal}Fatal]%{endif} - %{message}");
}

void createDirs()
{
    if (!QDir().mkpath(QStandardPaths::writableLocation(QStandardPaths::DataLocation)))
        qCritical() << "Could not create data directory '" + QStandardPaths::writableLocation(QStandardPaths::DataLocation) + "'";

    qDebug() << "Data path is:   " << QStandardPaths::writableLocation(QStandardPaths::DataLocation);
    qDebug() << "Config path is: " << QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <MDTK/db/abstractdatabaseclient.h>

using namespace MDTK;

QString AbstractDatabaseClient::filepath() const{
    return QStandardPaths::writableLocation(QStandardPaths::DataLocation) + "/" + name() + ".sqlite";
}

void AbstractDatabaseClient::init(){
    // init database connection
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE",name());
    db.setDatabaseName(filepath());
    if (!db.open()){
        printError("could not open database: " + db.lastError().text());
        return;
    }

    // configure tables
    const TableMap tables = this->tables();
    for (auto it = tables.begin(); it != tables.end(); it++){
        const QString &tableName = it.key();
        const TableSchemaHistory &tableSchemes = it.value();
        //printDebug("init table \""+tableName+"\"");

        QSqlQuery queryTableInfo("PRAGMA table_info("+tableName+")",db);
        if (queryTableInfo.at() == QSql::BeforeFirstRow) {
            // check if table has correct scheme
            QStringList existingColumns;
            while (queryTableInfo.next())
                existingColumns.append(queryTableInfo.value("name").toString());
            const int matchedScheme = matchScheme(tableName,existingColumns);
            if (matchedScheme == tableSchemes.size()-1) {
                // table has latest scheme -> nothing to be done
            } else if (matchedScheme >= 0) {
                // table has deprecated scheme and needs to be migrated
                requestMigration(tableName,matchedScheme,tableSchemes.size()-1,db);
            } else {
                // table has no known scheme -> quit programm
                printError("Table \""+tableName+"\" did not match any known scheme.");
                exit(1);
            }
        } else {
            createTable(tableName,it.value().back(),db);
        }
    }
}

bool AbstractDatabaseClient::createTable(const QString &tableName, const TableSchema &schema, QSqlDatabase &db){
    QString cmd = "CREATE TABLE "+tableName+" (";

    // look for autoincrement column
    bool foundAutoincrement = false;
    for (const TableColumn &col : schema){
        if (col.type == ColumnType::AUTOINCREMENT){
            cmd += col.name +" INTEGER PRIMARY KEY AUTOINCREMENT,";
            foundAutoincrement = true;
            break;
        }
    }
    if (!foundAutoincrement)
        cmd += "uid INTEGER PRIMARY KEY,";


    for (const TableColumn &col : schema){
        if (col.type == ColumnType::AUTOINCREMENT)
            continue;

        cmd += col.name + " ";
        switch (col.type) {
        case TEXT:
            cmd += "TEXT DEFAULT '"+col.defaultValue+"'";
            break;
        case INTEGER:
            cmd += "INTEGER DEFAULT "+(col.defaultValue.size()>0 ? col.defaultValue : "0");
            break;
        case REAL:
            cmd += "REAL DEFAULT "+(col.defaultValue.size()>0 ? col.defaultValue : "0");
            break;
        case DATE:
            cmd += "TEXT DEFAULT '"+col.defaultValue+"'";
            break;
        case MONTH:
            cmd += "TEXT DEFAULT '"+col.defaultValue+"'";
            break;
        case BOOLEAN:
            cmd += "TEXT DEFAULT '"+col.defaultValue+"'";
        }
        cmd += ",";
    }
    // remove last ","
    cmd.chop(1);
    cmd += ")";

    QSqlQuery q(cmd,db);
    return evaluateQuery(q,"when creating table \""+tableName+"\"");
}

QSqlQuery AbstractDatabaseClient::selectAll(const QString &tableName, QSqlDatabase &db, const QString &sortColumn, const bool ascending){
    QString cmd = "SELECT * FROM "+tableName;
    if (sortColumn.length() > 0)
        cmd += " ORDER BY " + sortColumn + " " + (ascending ? "ASC" : "DESC");
    QSqlQuery q(cmd,db);
    if (q.lastError().type())
        printError("selectAll() - could not execute query: " + q.lastError().text() + ". The query was:\n"+q.lastQuery());
    return q;
}

int AbstractDatabaseClient::simpleInsert(const QString &tableName, const QMap<QString, QString> values){
    QSqlDatabase db = openDatabase();
    if (!db.isOpen())
        return -1;

    QString cmd = "INSERT INTO "+tableName+" (";
    // add column names
    for(auto it = values.begin(); it != values.end(); it++){
        cmd += it.key();
        if (it.key() != values.lastKey())
            cmd += ",";
    }
    cmd += " ) VALUES (";
    for(auto it = values.begin(); it != values.end(); it++){
        cmd += it.value();
        if (it.key() != values.lastKey())
            cmd += ",";
    }
    cmd += " )";

    QSqlQuery q(cmd,db);
    if (q.lastError().type()){
        printError("insert into table '"+tableName+"' - could not execute query: " + q.lastError().text() + ". The query was:\n"+q.lastQuery());
        return -1;
    }
    bool ok;
    const int insertIdx = q.lastInsertId().toInt(&ok);
    return ok ? insertIdx : -1;
}

bool AbstractDatabaseClient::simpleUpdate(const QString &tableName, const QMap<QString, QString> values, const QString &key, const QString &keyColumnName){
    QSqlDatabase db = openDatabase();
    if (!db.isOpen())
        return false;

    QString cmd = "UPDATE "+tableName+" SET ";
    for(auto it = values.begin(); it != values.end(); it++){
        cmd += it.key() + "=" + it.value();
        if (it.key() != values.lastKey())
            cmd += ",";
    }
    cmd += " WHERE " + keyColumnName + "=" + key;

    QSqlQuery q(cmd,db);
    return evaluateQuery(q,"update table '"+tableName+"'");
}

bool AbstractDatabaseClient::simpleDelete(const QString &tableName, const QString &key, const QString &keyColumnName){
    return multiDelete(tableName,{{keyColumnName,key}});
}

bool AbstractDatabaseClient::multiDelete(const QString &tableName, const QMap<QString, QString> &conditions){
    // olny delete if there is at least one condition
    if (conditions.isEmpty())
        return false;

    QSqlDatabase db = openDatabase();
    if (!db.isOpen())
        return false;

    QString cmd = "DELETE FROM "+tableName+" WHERE ";
    for (auto it = conditions.begin(); it != conditions.end(); it++){
        cmd += it.key() +"="+it.value() + " ";
        if (it.key() != conditions.lastKey())
            cmd += "AND ";
    }

    QSqlQuery q(cmd,db);
    return evaluateQuery(q,"remove from table '"+tableName+"'");
}

bool AbstractDatabaseClient::evaluateQuery(QSqlQuery &q, const QString &context){
    if (q.lastError().type()){
        QString msg = context;
        if (msg.size()>0)
            msg += " - ";
        msg += "could not execute query: " + q.lastError().text() + ". The query was:\n"+q.lastQuery();
        printError(msg);
        return false;
    }
    return true;
}

bool AbstractDatabaseClient::hasTable(const QString &tableName, QSqlDatabase &db){
    QSqlQuery q("SELECT * FROM "+tableName,db);
    return q.isActive();
}

bool AbstractDatabaseClient::tableHasColumn(const QString &tableName, const QString columnName, QSqlDatabase &db){
    QSqlQuery q("PRAGMA table_info("+tableName+")",db);
    if (!q.isActive()){
        printError("tableHasColumn() - could not get table_info of table \""+tableName+"\": " + q.lastError().text());
        return false;
    }

    while (q.next())
        if (q.value("name") == columnName)
            return true;
    return false;
}

void AbstractDatabaseClient::printDebug(const QString &message) const {
    qDebug().noquote().nospace() << "Database \""+name()+"\" - "+message;
}
void AbstractDatabaseClient::printInfo(const QString &message) const {
    qInfo().noquote().nospace() << "Database \""+name()+"\" - "+message;
}
void AbstractDatabaseClient::printWarning(const QString &message) const {
    qWarning().noquote().nospace() << "Database \""+name()+"\" - "+message;
}
void AbstractDatabaseClient::printError(const QString &message) const {
    qCritical().noquote().nospace() << "Database \""+name()+"\" - "+message;
}


QString AbstractDatabaseClient::parseText(QSqlQuery &q, const QString &colName) const {
    return q.value(colName).toString();
}

int AbstractDatabaseClient::parseInteger(QSqlQuery &q, const QString &colName) const {
    return q.value(colName).toInt();
}

uint AbstractDatabaseClient::parseUInt(QSqlQuery &q, const QString &colName) const{
    return q.value(colName).toUInt();
}

qreal AbstractDatabaseClient::parseReal(QSqlQuery &q, const QString &colName) const {
    return q.value(colName).toReal();
}

QDate AbstractDatabaseClient::parseDate(QSqlQuery &q, const QString &colName) const {
    return QDate::fromString(q.value(colName).toString(),"yyyy/MM/dd");
}

QDate AbstractDatabaseClient::parseMonth(QSqlQuery &q, const QString &colName) const {
    return QDate::fromString(q.value(colName).toString(),"yyyy/MM");
}

bool AbstractDatabaseClient::parseBoolean(QSqlQuery &q, const QString &colName) const {
    return q.value(colName).toString() == "true";
}


QString AbstractDatabaseClient::writeText(const QString &value) const {
    return " '" + value + "' ";
}

QString AbstractDatabaseClient::writeInteger(const int value) const {
    return " " + QString::number(value) + " ";
}

QString AbstractDatabaseClient::writeUInt(const uint value) const {
    return " " + QString::number(value) + " ";
}

QString AbstractDatabaseClient::writeReal(const qreal value) const {
    return " " + QString::number(value) + " ";
}

QString AbstractDatabaseClient::writeDate(const QDate &value) const {
    return " '" + value.toString("yyyy/MM/dd") + "' ";
}

QString AbstractDatabaseClient::writeMonth(const Month &value) const {
    return " '" + value.toString("yyyy/MM") + "' ";
}

QString AbstractDatabaseClient::writeBoolean(const bool value) const {
    return value ? " 'true' " : " 'false' ";
}

int AbstractDatabaseClient::matchScheme(const QString &tableName, const QStringList &columns){
    const TableSchemaHistory schemes = tables()[tableName];
    // look for exact match
    for (int i=0; i<schemes.size(); i++){
        const TableSchema &currentScheme = schemes.at(i);
        bool currentSchemeHasAutoincrement = false;
        for (const TableColumn &col : currentScheme){
            if (col.type == ColumnType::AUTOINCREMENT){
                currentSchemeHasAutoincrement = true;
                break;
            }
        }

        // if col count doesn't match: continue;
        if (currentScheme.size() != (currentSchemeHasAutoincrement ? columns.size() : columns.size() - 1))
            continue;

        bool exactMatch = true;
        // each column from the scheme must be in columns
        QStringList currentColNames;
        for (const TableColumn &col : currentScheme){
            currentColNames.push_back(col.name);
            if (!columns.contains(col.name)){
                exactMatch = false;
                break;
            }
        }
        if (!exactMatch)
            continue;

        // each column in columns must be in the scheme
        for (const QString &colName : columns){
            if (colName == "uid")
                continue;
            if (!currentColNames.contains(colName)){
                exactMatch = false;
                break;
            }
        }
        if (exactMatch)
           return i;
    }


    // look for latest scheme that contains all required columns
    for (int i=schemes.size()-1; i>=0; i--){
        const TableSchema &currentScheme = schemes.at(i);
        // if col count doesn't match: continue;
        if (currentScheme.size() > columns.size() - 1)
            continue;

        bool match = true;
        // each column from the scheme must be in columns
        for (const TableColumn &col : currentScheme){
            if (!columns.contains(col.name)){
                match = false;
                break;
            }
        }
        if (match)
           return i;
    }
    return -1;
}

void AbstractDatabaseClient::requestMigration(const QString &tableName, const int fromScheme, const int toScheme, QSqlDatabase &db){
    TableMigrationEvent event;
    event.tableName  = tableName;
    event.fromScheme = fromScheme;
    event.toScheme   = toScheme;
    event.db         = &db;

    // make backup
    QFile::remove(filepath()+".migrationbackup");
    if (QFile::copy(filepath(),filepath()+".migrationbackup")){
        printInfo("created database backup: '"+filepath()+".migrationbackup'");
    } else {
        printError("could not create database backup at '"+filepath()+".migrationbackup'.");
        exit(1);
    }

    // this function needs to be reimplemented in any child class that has any scheme change
    handleMigrations(event);

    if (!event.handled){
        printError("Not supported migration requested: table \""+tableName+"\": "
                   +QString::number(fromScheme)+" -> "+QString::number(toScheme));
        exit(1);
    }
}

QSqlDatabase AbstractDatabaseClient::openDatabase(){
    QSqlDatabase db = QSqlDatabase::database(name());
    if (!db.open())
        printError("could not open database: " + db.lastError().text());
    return db;
}

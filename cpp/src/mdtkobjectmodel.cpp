/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MDTK/mdtkobjectmodel.h"

void MDTKObjectModel::setSortMode(const int sortMode)
{
    if (sortMode == m_sortMode)
        return;

    switch (sortMode) {
    case UNSORTED:
    case SORT_ASCENDING:
    case SORT_DESCENDING:
        m_sortMode = (SortMode) sortMode;
        this->resort();
        emit sortModeChanged();
        break;
    default:
        qWarning() << "MDTKObjectModel::setSortMode() - received unknown sortmode: " << sortMode << ". Do nothing.";
        break;
    }
}

QVariant MDTKObjectModel::data(const QModelIndex &index, int role) const
{
    const int row = index.row();
    if (row >= 0 && row < m_list.size()) {
        switch(role) {
        case OBJECTROLE:
            return QVariant::fromValue<MDTKObject*>(m_list.at(row));
        }
    }
    return QVariant();
}

QHash<int, QByteArray> MDTKObjectModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[OBJECTROLE] = this->objectRoleName().toUtf8();
    return names;
}

void MDTKObjectModel::clear()
{
    if (m_list.empty())
        return;

    beginResetModel();
    for (MDTKObject *obj : m_list){
        disconnect(obj,SIGNAL(rankingChanged(MDTKObject*)),this,SLOT(resort(MDTKObject*)));
        disconnect(obj,SIGNAL(destruction(MDTKObject*)),this,SLOT(doRemove(MDTKObject*)));
    }
    m_list.clear();
    endResetModel();
    emit countChanged();
}

void MDTKObjectModel::insertAt(MDTKObject *obj, const int idx)
{
    const int N = m_list.size();
    if (m_sortMode == UNSORTED){
        const int idxEff = idx >= 0 && idx < N ? idx : N;
        doInsertAt(idxEff,obj);
    } else {
        insert(obj);
    }
}

bool MDTKObjectModel::removeByUid(const uint uid)
{
    const int idx = m_list.find(uid);
    if (idx < 0)
        return false;

    MDTKObject *obj = m_list.at(idx);
    return remove(obj);
}

int MDTKObjectModel::getInsertIdx(const MDTKObject *obj) const
{
    const int N = m_list.size();
    if (m_sortMode == UNSORTED)
        return N;

    switch (m_sortMode) {
    case SORT_ASCENDING:
        for (int idx = 0; idx < N; idx++)
            if (!m_list.at(idx)->isLessThan(obj))
                return idx;
        break;
    case SORT_DESCENDING:
        for (int idx = 0; idx < N; idx++)
            if (m_list.at(idx)->isLessThan(obj))
                return idx;
        break;
    }
    return N;
}

void MDTKObjectModel::resort()
{
    // nothing to sort if empty or if unsorted
    if (m_list.empty() || m_sortMode == UNSORTED)
        return;

    // build new list
    QList<MDTKObject*> newList;
    switch (m_sortMode) {
    case SORT_ASCENDING:
        for (MDTKObject *obj : m_list){
            auto insertPos = newList.begin();
            // increment insert position as long as item at current position is < than obj
            while(insertPos != newList.end() && (*insertPos)->isLessThan(obj))
                insertPos++;
            newList.insert(insertPos,obj);
        }
        break;
    case SORT_DESCENDING:
        for (MDTKObject *obj : m_list){
            auto insertPos = newList.end();
            if (!newList.empty()) {
                // decrement insert position as long as item at preceeding position is < than obj
                while(insertPos != newList.begin() && (*(insertPos-1))->isLessThan(obj))
                    insertPos--;
                newList.insert(insertPos,obj);
            }
        }
        break;
    }

    // update model
    beginResetModel();
    m_list.clear();
    m_list.reserve(newList.size());
    for (MDTKObject* obj : newList)
        m_list.push_back(obj);
    endResetModel();
}

void MDTKObjectModel::doInsertAt(const int idx, MDTKObject *obj)
{
    beginInsertRows(QModelIndex(),idx,idx);
    m_list.insert(idx,obj);
    endInsertRows();
    connect(obj,SIGNAL(rankingChanged(MDTKObject*)),this,SLOT(resort(MDTKObject*)));
    connect(obj,SIGNAL(destruction(MDTKObject*)),this,SLOT(doRemove(MDTKObject*)));
    emit countChanged();
}

bool MDTKObjectModel::doRemove(MDTKObject *obj)
{
    const int N = m_list.size();
    for (int i=0; i<N; i++){
        if (*m_list.at(i) == *obj) {
            disconnect(m_list.at(i),SIGNAL(rankingChanged(MDTKObject*)),this,SLOT(resort(MDTKObject*)));
            disconnect(m_list.at(i),SIGNAL(destruction(MDTKObject*)),this,SLOT(doRemove(MDTKObject*)));
            beginRemoveRows(QModelIndex(),i,i);
            m_list.removeAt(i);
            endRemoveRows();
            emit countChanged();
            return true;
        }
    }
    return false;
}

void MDTKObjectModel::resort(MDTKObject *obj)
{
    // nothing to sort if empty or if unsorted
    if (m_list.empty() || m_sortMode == UNSORTED || !obj)
        return;

    // search object to resort
    const int N = m_list.size();
    int from = 0;
    while(*m_list.at(from) != *obj){
        // if we reached the end of the list, obj is not part of the model, i.e. nothing to be done
        if (from == N-1)
            return;
        // else, increment iterator
        from++;
    }
    int to = from;

    // try to decrease index of obj
    switch (m_sortMode) {
    case SORT_ASCENDING:
        // decrement "to" as long as its precessor is not < obj
        while (to > 0 && !m_list.at(to-1)->isLessThan(obj))
            to--;
        break;
    case SORT_DESCENDING:
        // decrement "to" as long as its precessor is < obj
        while (to > 0 && m_list.at(to-1)->isLessThan(obj))
            to--;
        break;
    }

    // try to increase index of obj
    if (to == from) {
        switch (m_sortMode) {
        case SORT_ASCENDING:
            // increment toIt as long as its successor is < obj
            while (to < N-1 && m_list.at(to+1)->isLessThan(obj))
                to++;
            break;
        case SORT_DESCENDING:
            // increment toIt as long as its successor is not < obj
            while (to < N-1 && !m_list.at(to+1)->isLessThan(obj))
                to++;
            break;
        }
    }

    // move obj
    if (to < from){
        beginMoveRows(QModelIndex(),from,from,QModelIndex(),to);
        m_list.move(from,to);
        endMoveRows();
    } else if (to > from) {
        beginMoveRows(QModelIndex(),from,from,QModelIndex(),to+1);
        m_list.move(from,to);
        endMoveRows();
    }
}

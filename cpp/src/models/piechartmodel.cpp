/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * finance is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <MDTK/models/piechartmodel.h>

// QAbstractItemModel interface
int PieChartModel::rowCount(const QModelIndex &parent) const {
    return m_list.size();
}

QVariant PieChartModel::data(const QModelIndex &index, int role) const {
    switch (role) {
    case UIDROLE:
        return QVariant(m_list.at(index.row()).uid);
    case COLORROLE:
        return QVariant(m_list.at(index.row()).color);
    case VALUEROLE:
        return QVariant(m_list.at(index.row()).value);
    case PRIORVALUESROLE:
        return QVariant(m_list.at(index.row()).priorValues);
    }
    return QVariant();
}

QHash<int, QByteArray> PieChartModel::roleNames() const {
    QHash<int, QByteArray> names;
    names[UIDROLE]   = "modelUid";
    names[COLORROLE] = "modelColor";
    names[VALUEROLE] = "modelValue";
    names[PRIORVALUESROLE] = "modelPriorValues";
    return names;
}

qreal PieChartModel::getValue(const uint uid) const{
    int idx = m_list.find(uid);
    return idx < 0 ? 0. : m_list.at(idx).value;
}


// C++ interface
void PieChartModel::addValue(const uint uid, const qreal value, const QColor &color) {
    int idx = m_list.find(uid);

    if (idx < 0) {
        PieChartModelNode n(uid,color,value);
        int insertIdx = 0;
        while (insertIdx < m_list.size() && m_list.at(insertIdx).value > value){
            n.priorValues += m_list.at(insertIdx).value;
            insertIdx++;
        }
        beginInsertRows(QModelIndex(),insertIdx,insertIdx);
        m_list.insert(insertIdx,n);
        endInsertRows();
        emit countChanged();
        updatePriors();
    } else {
        const qreal val = m_list[idx].value += value;
        m_list[idx].count += 1;
        emit dataChanged(index(idx),index(idx),{VALUEROLE});
        int newIdx = idx;
        while (newIdx > 0 && m_list.at(newIdx-1).value < val)
            newIdx--;
        if (newIdx != idx){
            beginMoveRows(QModelIndex(),idx,idx,QModelIndex(),newIdx);
            m_list.move(idx,newIdx);
            endMoveRows();
        }
        updatePriors();
    }
}

void PieChartModel::removeValue(const uint uid, const qreal value) {
    const int idx = m_list.find(uid);
    if (idx < 0)
        return;

    const qreal val = m_list[idx].value -= value;
    const int newCount = m_list[idx].count -= 1;

    if (newCount > 0) {
        emit dataChanged(index(idx),index(idx),{VALUEROLE});

        int newIdx = idx;
        while (newIdx < m_list.size()-1 && m_list.at(newIdx+1).value > val)
            newIdx++;
        if (newIdx != idx){
            m_list.move(idx,newIdx);
            emit dataChanged(index(idx),index(newIdx));
        }
    } else {
        beginRemoveRows(QModelIndex(),idx,idx);
        m_list.remove(idx);
        endRemoveRows();
        emit countChanged();
    }

    updatePriors();
}

void PieChartModel::updateColor(const uint uid, const QColor &color) {
    const int idx = m_list.find(uid);
    if (idx < 0)
        return;

    m_list[idx].color = color;
    emit dataChanged(index(idx),index(idx),{COLORROLE});
}

void PieChartModel::updatePriors(){
    qreal prior = 0.;
    for (int i=0; i<m_list.size(); i++){
        m_list[i].priorValues = prior;
        prior += m_list[i].value;
    }
    emit dataChanged(index(0),index(m_list.size()-1),{PRIORVALUESROLE});
    emit sumChanged();
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <MDTK/models/densemonthlyvaluesmodel.h>

DenseMonthlyValuesModel::DenseMonthlyValuesModel(QObject *parent, const QVector<QString> roles, const Month &firstMonth, const Month &lastMonth)
    : QAbstractListModel(parent), m_roles(roles), m_numRoles(m_roles.size())
{
    // init model
    m_list.reserve(lastMonth - firstMonth + 1);
    if (firstMonth.isValid() && lastMonth.isValid() && firstMonth <= lastMonth){
        for (Month curMonth = firstMonth; curMonth <= lastMonth; curMonth = curMonth.addMonths(1))
            m_list.append(Node(m_numRoles,curMonth));
    } else {
        m_list.append(Node(m_numRoles,Month::currentMonth()));
    }
}

int DenseMonthlyValuesModel::rowCount(const QModelIndex &) const{
    return m_list.size();
}

QVariant DenseMonthlyValuesModel::data(const QModelIndex &index, int role) const{
    const int idx = index.row();
    if (idx < 0 || idx >= m_list.size())
        return QVariant();
    const Node &node = m_list.at(idx);

    if (role == MONTHROLE)
        return QVariant(QDateTime(node.month.toDate()));
    if (role == COUNTSROLE)
        return QVariant(node.counts);

    const int roleIdx = role - VALUESROLE;
    if (roleIdx >= 0 && roleIdx < node.values.size())
        return QVariant(node.values.at(roleIdx));
    return QVariant();
}

QHash<int, QByteArray> DenseMonthlyValuesModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[MONTHROLE]  = "modelMonth";
    names[COUNTSROLE] = "modelCounts";
    for (int i=0; i<m_roles.size(); i++)
        names[VALUESROLE+i] = m_roles.at(i).toUtf8();
    return names;
}

void DenseMonthlyValuesModel::setFirstMonth(const Month &m, const bool extendOnly){
    const int monthsToAdd = m_firstMonth - m;

    if (monthsToAdd > 0){
        beginInsertRows(QModelIndex(),0,monthsToAdd);
        m_list.insert(0,monthsToAdd,Node(m_numRoles));
        for (int i=0; i<monthsToAdd; i++)
            m_list[i].month = m.addMonths(i);
        endInsertRows();
        emit countChanged();

        m_firstMonth = m;
        emit firstMonthChanged();

        if (m_valuesMin > 0){
            m_valuesMin = 0;
            emit valuesMinChanged();
        }

    } else if (!extendOnly && monthsToAdd < 0) {
        const int monthsToRemove = -monthsToAdd;
        beginRemoveRows(QModelIndex(),0,monthsToRemove);
        for (int i=0; i<monthsToRemove; i++)
            m_list.remove(0,monthsToRemove);
        endRemoveRows();
        emit countChanged();

        m_firstMonth = m;
        emit firstMonthChanged();
        updateExtremeValues();
    }
}

void DenseMonthlyValuesModel::setLastMonth(const Month &m, const bool extendOnly){
    const int monthsToAdd = m - m_lastMonth;
    const int N = m_list.size();

    if (monthsToAdd > 0){
        beginInsertRows(QModelIndex(),N,N+monthsToAdd-1);
        m_list.append(QVector<Node>(monthsToAdd,Node(m_numRoles)));
        for (int i=0; i<monthsToAdd; i++)
            m_list[N+i].month = m_lastMonth.addMonths(1+i);
        endInsertRows();
        emit countChanged();

        m_lastMonth = m;
        emit lastMonthChanged();

        if (m_valuesMin > 0){
            m_valuesMin = 0;
            emit valuesMinChanged();
        }

    } else if (!extendOnly && monthsToAdd < 0) {
        const int monthsToRemove = -monthsToAdd;
        beginRemoveRows(QModelIndex(),N-1-monthsToRemove,N-1);
        m_list.remove(N-1-monthsToRemove,monthsToRemove);
        endRemoveRows();
        emit countChanged();

        m_lastMonth = m;
        emit lastMonthChanged();
        updateExtremeValues();
    }
}

void DenseMonthlyValuesModel::addValues(const Month &m, const QVector<qreal> &values){
    // extend time range if needed
    setFirstMonth(m,true);
    setLastMonth(m,true);

    const int idx = m - m_firstMonth;
    Node &node = m_list[idx];
    node.counts += 1;
    const int numValues = qMin(values.size(),node.values.size());
    QVector<int> changedRoles = {COUNTSROLE};
    for (int i=0; i<numValues; i++){
        node.values[i] += values[i];
        changedRoles.push_back(VALUESROLE+i);
    }
    emit dataChanged(index(idx),index(idx),changedRoles);
}

void DenseMonthlyValuesModel::removeValues(const Month &m, const QVector<qreal> &values){
    if (m < m_firstMonth || m > m_lastMonth)
        return;

    const int idx = m - m_firstMonth;
    Node &node = m_list[idx];
    node.counts -= 1;
    const int numValues = qMin(values.size(),node.values.size());
    QVector<int> changedRoles = {COUNTSROLE};
    for (int i=0; i<numValues; i++){
        node.values[i] -= values[i];
        changedRoles.push_back(VALUESROLE+i);
    }
    emit dataChanged(index(idx),index(idx),changedRoles);
}

void DenseMonthlyValuesModel::updateExtremeValues(){
    qreal newMin = 0.;
    qreal newMax = 0.;

    if (m_list.size() > 0){
        newMax = newMin = m_list.begin()->values.at(0);
        for (const Node &node : m_list){
            for (const qreal val : node.values){
                if (val < newMin)
                    newMin = newMin;
                else
                    newMax = qMax(newMax,val);
            }
        }
    }

    if (m_valuesMin != newMin){
        m_valuesMin = newMin;
        emit valuesMinChanged();
    }

    if (m_valuesMax != newMax){
        m_valuesMax = newMax;
        emit valuesMaxChanged();
    }
}

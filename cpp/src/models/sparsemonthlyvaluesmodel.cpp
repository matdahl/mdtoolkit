/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <MDTK/models/sparsemonthlyvaluesmodel.h>

int SparseMonthlyValuesModel::rowCount(const QModelIndex &) const{
    return m_list.size();
}

QVariant SparseMonthlyValuesModel::data(const QModelIndex &index, int role) const{
    const int idx = index.row();
    if (idx < 0 || idx >= m_list.size())
        return QVariant();
    const Node &node = m_list.at(idx);

    if (role == MONTHROLE)
        return QVariant(QDateTime(node.month.toDate()));
    if (role == COUNTSROLE)
        return QVariant(node.counts);

    const int roleIdx = role - VALUESROLE;
    if (roleIdx >= 0 && roleIdx < node.values.size())
        return QVariant(node.values.at(roleIdx));
    return QVariant();
}

QHash<int, QByteArray> SparseMonthlyValuesModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[MONTHROLE]  = "modelMonth";
    names[COUNTSROLE] = "modelCounts";
    for (int i=0; i<m_roles.size(); i++)
        names[VALUESROLE+i] = m_roles.at(i).toUtf8();
    return names;
}

bool SparseMonthlyValuesModel::hasMonth(const QDateTime &month) const{
    const Month refMonth(month.date());
    for (const Node &node : m_list)
        if (node.month == refMonth)
            return true;
    return false;
}

void SparseMonthlyValuesModel::addValues(const Month &m, const QVector<qreal> &values){
    const int idx = getIndex(m);
    Node &node = m_list[idx];
    node.counts += 1;
    const int numValues = qMin(values.size(),node.values.size());
    QVector<int> changedRoles = {COUNTSROLE};
    for (int i=0; i<numValues; i++){
        node.values[i] += values[i];
        changedRoles.push_back(VALUESROLE+i);
    }
    emit dataChanged(index(idx),index(idx),changedRoles);
}

void SparseMonthlyValuesModel::removeValues(const Month &m, const QVector<qreal> &values){
    const int idx = getIndex(m,false);
    if (idx < 0)
        return;

    Node &node = m_list[idx];
    node.counts -= 1;
    const int numValues = qMin(values.size(),node.values.size());
    QVector<int> changedRoles = {COUNTSROLE};
    for (int i=0; i<numValues; i++){
        node.values[i] -= values[i];
        changedRoles.push_back(VALUESROLE+i);
    }
    emit dataChanged(index(idx),index(idx),changedRoles);

    if (node.counts < 1){
        beginRemoveRows(QModelIndex(),idx,idx);
        m_list.remove(idx);
        endRemoveRows();
        emit countChanged();
        if (idx == 0)
            emit firstMonthChanged();
        if (idx == m_list.size())
            emit lastMonthChanged();
    }
}

int SparseMonthlyValuesModel::getIndex(const Month &month, bool insertIfNotExists){
    int idx;
    const int N = m_list.size();
    for (idx = 0; idx<N; idx++){
        if (m_list.at(idx).month == month)
            return idx;
        if (m_list.at(idx).month > month)
            break;
    }

    // insert new node
    if (insertIfNotExists){
        beginInsertRows(QModelIndex(),idx,idx);
        m_list.insert(idx,Node(m_numRoles,month));
        endInsertRows();
        emit countChanged();
        if (idx == 0)
            emit firstMonthChanged();
        if (idx == m_list.size()-1)
            emit lastMonthChanged();
        return idx;
    }
    return -1;
}

void SparseMonthlyValuesModel::updateExtremeValues(){
    qreal newMin = 0.;
    qreal newMax = 0.;

    if (m_list.size() > 0){
        newMax = newMin = m_list.begin()->values.at(0);
        for (const Node &node : m_list){
            for (const qreal val : node.values){
                if (val < newMin)
                    newMin = newMin;
                else
                    newMax = qMax(newMax,val);
            }
        }
    }

    if (m_valuesMin != newMin){
        m_valuesMin = newMin;
        emit valuesMinChanged();
    }

    if (m_valuesMax != newMax){
        m_valuesMax = newMax;
        emit valuesMaxChanged();
    }
}

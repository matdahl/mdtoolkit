/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <MDTK/models/sparseannualvaluesmodel.h>

int SparseAnnualValuesModel::rowCount(const QModelIndex &) const{
    return m_list.size();
}

QVariant SparseAnnualValuesModel::data(const QModelIndex &index, int role) const{
    const int idx = index.row();
    if (idx < 0 || idx >= m_list.size())
        return QVariant();
    const Node &node = m_list.at(idx);

    if (role == YEARROLE)
        return QVariant(node.year);
    if (role == COUNTSROLE)
        return QVariant(node.counts);

    const int roleIdx = role - VALUESROLE;
    if (roleIdx >= 0 && roleIdx < node.values.size())
        return QVariant(node.values.at(roleIdx));
    return QVariant();
}

QHash<int, QByteArray> SparseAnnualValuesModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[YEARROLE]   = "modelYear";
    names[COUNTSROLE] = "modelCounts";
    for (int i=0; i<m_roles.size(); i++)
        names[VALUESROLE+i] = m_roles.at(i).toUtf8();
    return names;
}

bool SparseAnnualValuesModel::hasYear(const int year) const{
    for (const Node &node : m_list)
        if (node.year == year)
            return true;
    return false;
}

void SparseAnnualValuesModel::addValues(const int y, const QVector<qreal> &values){
    const int idx = getIndex(y);
    Node &node = m_list[idx];
    node.counts += 1;
    const int numValues = qMin(values.size(),node.values.size());
    QVector<int> changedRoles = {COUNTSROLE};
    for (int i=0; i<numValues; i++){
        node.values[i] += values[i];
        changedRoles.push_back(VALUESROLE+i);
    }
    emit dataChanged(index(idx),index(idx),changedRoles);
}

void SparseAnnualValuesModel::removeValues(const int y, const QVector<qreal> &values){
    const int idx = getIndex(y,false);
    if (idx < 0)
        return;

    Node &node = m_list[idx];
    node.counts -= 1;
    const int numValues = qMin(values.size(),node.values.size());
    QVector<int> changedRoles = {COUNTSROLE};
    for (int i=0; i<numValues; i++){
        node.values[i] -= values[i];
        changedRoles.push_back(VALUESROLE+i);
    }
    emit dataChanged(index(idx),index(idx),changedRoles);

    if (node.counts < 1){
        beginRemoveRows(QModelIndex(),idx,idx);
        m_list.remove(idx);
        endRemoveRows();
        if (idx == 0)
            emit firstYearChanged();
        if (idx == m_list.size())
            emit lastYearChanged();
        emit countChanged();
    }
}

int SparseAnnualValuesModel::getIndex(const int year, bool insertIfNotExists){
    int idx;
    const int N = m_list.size();
    for (idx = 0; idx<N; idx++){
        if (m_list.at(idx).year == year)
            return idx;
        if (m_list.at(idx).year > year)
            break;
    }

    // insert new node
    if (insertIfNotExists){
        beginInsertRows(QModelIndex(),idx,idx);
        m_list.insert(idx,Node(m_numRoles,year));
        endInsertRows();
        emit countChanged();
        if (idx == 0)
            emit firstYearChanged();
        if (idx == m_list.size()-1)
            emit lastYearChanged();
        return idx;
    }
    return -1;
}

void SparseAnnualValuesModel::updateExtremeValues(){
    qreal newMin = 0.;
    qreal newMax = 0.;

    if (m_list.size() > 0){
        newMax = newMin = m_list.begin()->values.at(0);
        for (const Node &node : m_list){
            for (const qreal val : node.values){
                if (val < newMin)
                    newMin = newMin;
                else
                    newMax = qMax(newMax,val);
            }
        }
    }

    if (m_valuesMin != newMin){
        m_valuesMin = newMin;
        emit valuesMinChanged();
    }

    if (m_valuesMax != newMax){
        m_valuesMax = newMax;
        emit valuesMaxChanged();
    }
}

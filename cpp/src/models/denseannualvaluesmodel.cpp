/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <MDTK/models/denseannualvaluesmodel.h>

DenseAnnualValuesModel::DenseAnnualValuesModel(QObject *parent, const QVector<QString> roles, const int firstYear, const int lastYear)
    : QAbstractListModel(parent), m_roles(roles), m_numRoles(m_roles.size())
{
    // init model
    m_list.reserve(lastYear - firstYear + 1);
    if (firstYear <= lastYear){
        for (int curYear = firstYear; curYear <= lastYear; curYear++)
            m_list.append(Node(m_numRoles,curYear));
    } else {
        m_list.append(Node(m_numRoles,QDate::currentDate().year()));
    }
}

int DenseAnnualValuesModel::rowCount(const QModelIndex &) const{
    return m_list.size();
}

QVariant DenseAnnualValuesModel::data(const QModelIndex &index, int role) const{
    const int idx = index.row();
    if (idx < 0 || idx >= m_list.size())
        return QVariant();
    const Node &node = m_list.at(idx);

    if (role == YEARROLE)
        return QVariant(node.year);
    if (role == COUNTSROLE)
        return QVariant(node.counts);

    const int roleIdx = role - VALUESROLE;
    if (roleIdx >= 0 && roleIdx < node.values.size())
        return QVariant(node.values.at(roleIdx));
    return QVariant();
}

QHash<int, QByteArray> DenseAnnualValuesModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[YEARROLE]   = "modelYear";
    names[COUNTSROLE] = "modelCounts";
    for (int i=0; i<m_roles.size(); i++)
        names[VALUESROLE+i] = m_roles.at(i).toUtf8();
    return names;
}

void DenseAnnualValuesModel::setFirstYear(const int y, const bool extendOnly){
    const int yearsToAdd = m_firstYear - y;

    if (yearsToAdd > 0){
        beginInsertRows(QModelIndex(),0,yearsToAdd);
        m_list.insert(0,yearsToAdd,Node(m_numRoles));
        for (int i=0; i<yearsToAdd; i++)
            m_list[i].year = y+i;
        endInsertRows();
        emit countChanged();

        m_firstYear = y;
        emit firstYearChanged();

        if (m_valuesMin > 0){
            m_valuesMin = 0;
            emit valuesMinChanged();
        }

    } else if (!extendOnly && yearsToAdd < 0) {
        const int yearsToRemove = -yearsToAdd;
        beginRemoveRows(QModelIndex(),0,yearsToRemove);
        for (int i=0; i<yearsToRemove; i++)
            m_list.remove(0,yearsToRemove);
        endRemoveRows();
        emit countChanged();

        m_firstYear = y;
        emit firstYearChanged();
        updateExtremeValues();
    }
}

void DenseAnnualValuesModel::setLastYear(const int y, const bool extendOnly){
    const int yearsToAdd = y - m_lastYear;
    const int N = m_list.size();

    if (yearsToAdd > 0){
        beginInsertRows(QModelIndex(),N,N+yearsToAdd-1);
        m_list.append(QVector<Node>(yearsToAdd,Node(m_numRoles)));
        for (int i=0; i<yearsToAdd; i++)
            m_list[N+i].year = m_lastYear + 1 + i;
        endInsertRows();
        emit countChanged();

        m_lastYear = y;
        emit lastYearChanged();

        if (m_valuesMin > 0){
            m_valuesMin = 0;
            emit valuesMinChanged();
        }

    } else if (!extendOnly && yearsToAdd < 0) {
        const int yearsToRemove = -yearsToAdd;
        beginRemoveRows(QModelIndex(),N-1-yearsToRemove,N-1);
        m_list.remove(N-1-yearsToRemove,yearsToRemove);
        endRemoveRows();
        emit countChanged();

        m_lastYear = y;
        emit lastYearChanged();
        updateExtremeValues();
    }
}

void DenseAnnualValuesModel::addValues(const int y, const QVector<qreal> &values){
    // extend time range if needed
    setFirstYear(y,true);
    setLastYear(y,true);

    const int idx = y - m_firstYear;
    Node &node = m_list[idx];
    node.counts += 1;
    const int numValues = qMin(values.size(),node.values.size());
    QVector<int> changedRoles = {COUNTSROLE};
    for (int i=0; i<numValues; i++){
        node.values[i] += values[i];
        changedRoles.push_back(VALUESROLE+i);
    }
    emit dataChanged(index(idx),index(idx),changedRoles);
}

void DenseAnnualValuesModel::removeValues(const int y, const QVector<qreal> &values){
    if (y < m_firstYear || y > m_lastYear)
        return;

    const int idx = y - m_firstYear;
    Node &node = m_list[idx];
    node.counts -= 1;
    const int numValues = qMin(values.size(),node.values.size());
    QVector<int> changedRoles = {COUNTSROLE};
    for (int i=0; i<numValues; i++){
        node.values[i] -= values[i];
        changedRoles.push_back(VALUESROLE+i);
    }
    emit dataChanged(index(idx),index(idx),changedRoles);
}

void DenseAnnualValuesModel::updateExtremeValues(){
    qreal newMin = 0.;
    qreal newMax = 0.;

    if (m_list.size() > 0){
        newMax = newMin = m_list.begin()->values.at(0);
        for (const Node &node : m_list){
            for (const qreal val : node.values){
                if (val < newMin)
                    newMin = newMin;
                else
                    newMax = qMax(newMax,val);
            }
        }
    }

    if (m_valuesMin != newMin){
        m_valuesMin = newMin;
        emit valuesMinChanged();
    }

    if (m_valuesMax != newMax){
        m_valuesMax = newMax;
        emit valuesMaxChanged();
    }
}

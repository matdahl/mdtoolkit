/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <MDTK/models/mdtkcolormodel.h>

int MDTKColorModel::rowCount(const QModelIndex &parent) const {
    return m_list.size();
}

QVariant MDTKColorModel::data(const QModelIndex &index, int role) const{
    switch (role) {
    case COLORROLE:
        return QVariant(m_list.at(index.row()).color);
    case COUNTSROLE:
        return QVariant(m_list.at(index.row()).counts);
    }
    return QVariant();
}

QHash<int, QByteArray> MDTKColorModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[COLORROLE]  = "modelColor";
    names[COUNTSROLE] = "modelCounts";
    return names;
}

QColor MDTKColorModel::colorAt(const int idx) const{
    if (idx < 0 || idx > m_list.size())
        return QColor();
    return m_list.at(idx).color;
}

void MDTKColorModel::insertColor(const int idx, const QColor &color){
    const int insertIdx = qMax(0,qMin(m_list.size(),idx));

    beginInsertRows(QModelIndex(),insertIdx,insertIdx);
    m_list.insert(insertIdx,Node(color));
    endInsertRows();
    emit countChanged();
}

void MDTKColorModel::replaceColor(const int idx, const QColor &color){
    if (idx < 0 || idx >= m_list.size())
        return;

    m_list[idx].color = color;
    emit dataChanged(index(idx),index(idx),{COLORROLE});
}

void MDTKColorModel::removeColor(const int idx){
    if (idx < 0 || idx >= m_list.size())
        return;

    beginRemoveRows(QModelIndex(),idx,idx);
    m_list.removeAt(idx);
    endRemoveRows();
    emit countChanged();
}

void MDTKColorModel::appendColor(const QColor &color){
    const int insertIdx = m_list.size();

    beginInsertRows(QModelIndex(),insertIdx,insertIdx);
    m_list.append(Node(color));
    endInsertRows();
    emit countChanged();
}

void MDTKColorModel::clear(){
    beginResetModel();
    m_list.clear();
    endResetModel();
    emit countChanged();
}

void MDTKColorModel::resize(const int count){
    if (m_list.size() == count)
        return;

    if (m_list.size() < count){
        beginInsertRows(QModelIndex(),m_list.size(),count-1);
        m_list.append(QVector<Node>(count-m_list.size()));
        endInsertRows();
    } else {
        beginRemoveRows(QModelIndex(),count,m_list.size()-1);
        m_list.resize(count);
        endRemoveRows();
    }
    emit countChanged();
}

void MDTKColorModel::incCounter(const int idx){
    if (idx < 0 || idx >= m_list.size())
        return;

    m_list[idx].counts += 1;
    emit dataChanged(index(idx),index(idx),{COUNTSROLE});
}

void MDTKColorModel::decCounter(const int idx){
    if (idx < 0 || idx >= m_list.size())
        return;

    m_list[idx].counts -= 1;
    emit dataChanged(index(idx),index(idx),{COUNTSROLE});
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <MDTK/models/sparsedailycountsmodel.h>

using namespace MDTK;

int SparseDailyCountsModel::rowCount(const QModelIndex &parent) const {
    return m_map.size();
}

QVariant SparseDailyCountsModel::data(const QModelIndex &index, int role) const{
    const auto it = m_map.begin() + index.row();
    switch (role) {
    case DATEROLE:
        return QVariant(QDateTime(it.key()));
    case COUNTSROLE:
        return QVariant(it.value());
    }
    return QVariant();
}

QHash<int, QByteArray> SparseDailyCountsModel::roleNames() const{
    QHash<int, QByteArray> names;
    names[DATEROLE]   = "modelDate";
    names[COUNTSROLE] = "modelCount";
    return names;
}

int SparseDailyCountsModel::getCounts(const QDate &date) const{
    const auto it = m_map.find(date);
    return it != m_map.end() ? it.value() : 0;
}

void SparseDailyCountsModel::addCounts(const QDate &date, const int value){
    if (m_map.isEmpty()){
        m_map[date] = value;
    } else {
        int idx = 0;
        // get iterator to position where given date is or will be inserted
        const auto itLowerBound = m_map.lowerBound(date);
        for (auto it = m_map.begin(); it != itLowerBound; it++)
            idx++;
        if (itLowerBound.key() == date){
            itLowerBound.value() += value;
            emit dataChanged(index(idx),index(idx),{COUNTSROLE});
        } else {
            beginInsertRows(QModelIndex(),idx,idx);
            m_map[date] = value;
            endInsertRows();
            emit countChanged();
        }
        m_sum += value;
        emit sumChanged();
    }
}

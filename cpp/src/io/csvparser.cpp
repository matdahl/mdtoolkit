/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * account-book is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <MDTK/io/csvparser.h>

QStringList CSVParser::parseLine(const QString &line, const QChar &delimiter){
    QStringList cols;

    bool  isEscaped = false;
    bool  isInQuotationMode = false;
    QChar quotationChar;
    QString currentCol;

    for (const QChar &c : line){
        if (!isEscaped && c == '\\'){
            // enter escaped mode and continue with next char
            isEscaped = true;
            continue;
        }

        if (isInQuotationMode){
            if (!isEscaped && c == quotationChar){
                // quit quotation mode
                isInQuotationMode = false;
            } else {
                // append char to column
                currentCol.append(c);
            }
        } else {
            if (c == '"' || c == '\''){
                // enter quotation mode
                isInQuotationMode = true;
                quotationChar = c;
            } else if (c == delimiter){
                // finish column
                cols.push_back(currentCol);
                currentCol.clear();
            } else {
                // append char to column
                currentCol.push_back(c);
            }
        }
        // escape mode is only valid for one char
        isEscaped = false;
    }

    // if last char was a "\", this must be added
    if (isEscaped)
        currentCol.push_back('\\');

    // write last column to list
    if (!currentCol.isEmpty())
        cols.push_back(currentCol);

    return cols;
}

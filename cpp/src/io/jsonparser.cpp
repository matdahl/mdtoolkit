/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <MDTK/io/jsonparser.h>

#include <QByteArray>
#include <QDebug>
#include <QFile>
#include <QStringList>

namespace MDTK {

QJsonDocument JSONParser::readFile(const QString &path)
{
    QFile f(path);
    if (!f.open(QIODevice::ReadOnly | QIODevice::Text)){
        qWarning().nospace() << "JSONParser::readFile() - Could not open file " << path << ".";
        return QJsonDocument();
    }

    const QByteArray bytes = f.readAll();
    f.close();

    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(bytes,&err);
    if (err.error != QJsonParseError::NoError){
        // get line+pos of error
        int line = 1;
        int pos = 0;
        auto it=bytes.begin();
        for (int i=0; i<err.offset; i++){
            if (*(it++) == '\n'){
                line++;
                pos = 0;
            } else {
                pos++;
            }
        }
        qWarning().noquote().nospace() << "JSONParser::readFile() - Error when parsing file \"" << path << "\" at line "
                                       << line << ":" << pos << ": " << err.errorString();
    }

    return doc;
}

} // namespace MDTK

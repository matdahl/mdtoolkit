/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * finance is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <MDTK/date/month.h>

const QDate Month::futureInfinity = QDate(9999,1,1);

Month::Month(const QDate &date) : m_month(date.month()), m_year(date.year()), m_isValid(date.isValid())
{}

Month::Month(const int year, const int month, const bool isValid)
    : m_month(month), m_year(year), m_isValid(isValid){
    while(m_month < 1){
        m_year--;
        m_month += 12;
    }
    while(m_month > 12){
        m_year++;
        m_month -= 12;
    }
}

Month::Month() : m_month(0), m_year(0), m_isValid(false) {}

Month Month::addMonths(const int m) const{
    return Month(m_year, m_month + m);
}

Month Month::addYears(const int y) const{
    return Month(m_year + y, m_month);
}

QDate Month::toDate(const int day) const{
    if (!m_isValid)
        return QDate();

    QDate date = QDate(m_year,m_month,1);
    if (day >= 0)
        return date.addDays(day-1);
    else
        return date.addMonths(1).addDays(day);
}

QDateTime Month::toDateTime(const int day) const {
    return QDateTime(toDate(day));
}

QString Month::toString(const QString &format) const{
    return toDate().toString(format);
}

Month::operator QString() const{
    return toString("MM/yyyy");
}

bool operator==(const Month &a, const Month &b){
    return a.month() == b.month() && a.year() == b.year();
}
bool operator==(const Month &a, const QDate &b){
    return a.month() == b.month() && a.year() == b.year();
}
bool operator==(const QDate &a, const Month &b){
    return a.month() == b.month() && a.year() == b.year();
}

bool operator!=(const Month &a, const Month &b){
    return !(a.month() == b.month() && a.year() == b.year());
}
bool operator!=(const Month &a, const QDate &b){
    return !(a.month() == b.month() && a.year() == b.year());
}
bool operator!=(const QDate &a, const Month &b){
    return !(a.month() == b.month() && a.year() == b.year());
}

bool operator>(const Month &a, const Month &b){
    return a.year() > b.year() || (a.year() == b.year() && a.month() > b.month());
}
bool operator>(const Month &a, const QDate &b){
    return a.year() > b.year() || (a.year() == b.year() && a.month() > b.month());
}
bool operator>(const QDate &a, const Month &b){
    return a.year() > b.year() || (a.year() == b.year() && a.month() > b.month());
}

bool operator<(const Month &a, const Month &b){
    return a.year() < b.year() || (a.year() == b.year() && a.month() < b.month());
}
bool operator<(const Month &a, const QDate &b){
    return a.year() < b.year() || (a.year() == b.year() && a.month() < b.month());
}
bool operator<(const QDate &a, const Month &b){
    return a.year() < b.year() || (a.year() == b.year() && a.month() < b.month());
}

bool operator>=(const Month &a, const Month &b){
    return a.year() > b.year() || (a.year() == b.year() && a.month() >= b.month());
}
bool operator>=(const Month &a, const QDate &b){
    return a.year() > b.year() || (a.year() == b.year() && a.month() >= b.month());
}
bool operator>=(const QDate &a, const Month &b){
    return a.year() > b.year() || (a.year() == b.year() && a.month() >= b.month());
}

bool operator<=(const Month &a, const Month &b){
    return a.year() < b.year() || (a.year() == b.year() && a.month() <= b.month());
}
bool operator<=(const Month &a, const QDate &b){
    return a.year() < b.year() || (a.year() == b.year() && a.month() <= b.month());
}
bool operator<=(const QDate &a, const Month &b){
    return a.year() < b.year() || (a.year() == b.year() && a.month() <= b.month());
}

int operator-(const Month &a, const Month &b){
    return 12*(a.year() - b.year()) + (a.month() - b.month());
}

Month operator+(const Month &a, const int &b){
    return a.addMonths(b);
}

Month operator-(const Month &a, const int &b){
    return a.addMonths(-b);
}

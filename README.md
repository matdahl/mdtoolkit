# mdtoolkit

This is a collection of basic C++ classes and QML components build on top of the [Lomiri UI Toolkit](https://gitlab.com/ubports/development/core/lomiri-ui-toolkit) that are frequently used in my apps for Ubunt Touch.

## Usage

The idea is to include this repository as a submodule into other Qt projects.

In the following, it is assumed that this repository is mounted as a submodule into the project directory at the path **libs/mdtoolkit**:

```bash
git submodule add https://gitlab.com/matdahl/mdtoolkit libs/mdtoolkit
```

### Include QML module (CMake)

The QML modules can be included to a project by including the following lines to the projects `CMakeLists.txt` (assuming all submodules should be included):

```cmake
set(MDTK_USE_CORE     ON)
set(MDTK_USE_EXPORTS  ON)
set(MDTK_USE_MANUAL   ON)
set(MDTK_USE_SETTINGS ON)
add_subdirectory(libs/mdtoolkit)
```

In order to include MDTK files also to the translations, these statements should be placed above the `include(ci/cmake/translations.cmake)` statement.

This script assumes that the variables `DATA_DIR` and `QT_IMPORTS_DIR` are set when calling `add_subdirectory()`.

### Include QML modules (Clickable)

To include the QML module to a clickable project, the following lines to the *clickable.yaml*
```yaml
install_qml:
  - ${ROOT}/libs/mdtoolkit/qml/MDTK
```

When doing this, all of the following modules become available:

```qml
import MDTK 1.0
import MDTK.Exports 1.0
import MDTK.Manual 1.0
import MDTK.Settings 1.0
```

**Note**: When using QtCreator via clickable, i.e. using `clickable ide qtcreator`,
the generated project does not import the QML modules by default. To do so, the following steps
need to be appended to the build pipeline manually:
```bash
mkdir -p %{buildDir}/install/lib/<arch-triplet>
cp -r %{sourceDir}/libs/mdtoolkit/qml/MDTK %{buildDir}/install/lib/<arch-triplet>
```

### Include C++ classes (CMake)

To include the C++ classes to a project, the include directories and source files needs to be added to the target.
Assuming the target that uses the classes is `${PLUGIN}`, then the *CMakeLists.txt* should contain the following:
```cmake
set(MDTK_SRC_DIR ${CMAKE_SOURCE_DIR}/libs/mdtoolkit/src)
file(GLOB_RECURSE MDTK_SRC
    ${MDTK_SRC_DIR}/*.cpp
)
include_directories(${MDTK_SRC_DIR})

add_library(${PLUGIN} MODULE ${SRC} ${MDTK_SRC})
```

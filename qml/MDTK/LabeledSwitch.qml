/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Item {
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width
    height: implicitHeight
    implicitHeight: label.height

    property alias title: label.text
    property alias textColor: label.color
    property alias checked: sw.checked
    property bool  disableWhenUnchecked: false

    readonly property int minimalWidth: label.x + label.implicitWidth + sw.width

    property var leadingComponent: Component{
        Item{}
    }

    Loader{
        id: loaderLeadingItem
        anchors.verticalCenter: parent.verticalCenter
        sourceComponent: root.leadingComponent
    }

    Label{
        id: label
        anchors{
            verticalCenter: parent.verticalCenter
            left: loaderLeadingItem.right
            right: sw.left
            leftMargin: loaderLeadingItem.width > 0 ? units.gu(1) : 0
            rightMargin: units.gu(1)
        }
        color: theme.palette[root.checked || !root.disableWhenUnchecked ? "normal" : "disabled"].backgroundText
        wrapMode: Label.Wrap
    }

    Switch{
        id: sw
        anchors{
            verticalCenter: parent.verticalCenter
            right: parent.right
        }
    }
}

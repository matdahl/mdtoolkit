/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Item {
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width
    height: childrenRect.height

    property int spacing: units.gu(2)
    readonly property int buttonWidth: (width-spacing)/2

    signal cancelled()
    signal accepted()

    property alias cancelText: btCancel.text
    property alias acceptText: btAccept.text
    property alias acceptColor: btAccept.color
    property alias acceptable:  btAccept.enabled

    Button{
        id: btCancel
        anchors.left: parent.left
        text: i18n.tr("Cancel")
        width: root.buttonWidth
        onClicked: {
            root.cancelled()
            if (PopupUtils && dialog)
                PopupUtils.close(dialog)
        }
    }

    Button{
        id: btAccept
        anchors.right: parent.right
        text: i18n.tr("Ok")
        width: root.buttonWidth
        color: UbuntuColors.orange
        onClicked: {
            root.accepted()
            if (PopupUtils && dialog)
                PopupUtils.close(dialog)
        }
    }
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Button {
    id: root

    property alias _iconName: icon.name
    property alias _iconSource: icon.source
    property alias _iconColor: icon.color

    property alias _text:      label.text
    property alias _textColor: label.color

    Row{
        anchors.centerIn: parent
        height: units.gu(2)
        spacing: units.gu(1)

        Icon{
            id: icon
            height: parent.height
            color: root._textColor
        }

        Label{
            id: label
            anchors.verticalCenter: parent.verticalCenter
            color: theme.palette.normal.baseText
        }
    }
}

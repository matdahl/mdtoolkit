/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12

Flickable {
    id: root
    anchors.fill: parent
    contentHeight: Math.max(height, topFlickMargin + columnHeight + bottomFlickMargin)

    default property alias contents: column.data
    property alias padding: column.padding
    property alias spacing: column.spacing
    readonly property alias columnContentWidth: column.contentWidth
    readonly property alias columnWidth: column.width
    readonly property alias columnHeight: column.height

    property int topFlickMargin: 0
    property int bottomFlickMargin: 0

    Column{
        id: column
        y: root.topFlickMargin
        width: root.width
        readonly property int contentWidth: width - 2*padding
    }
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Item {
    id: root
    anchors.fill: parent
    visible: isBusy

    property bool isBusy: false
    property bool showProgress: false

    property alias title: lbTitle.text
    property alias progressMessage: lbProgress.text

    property alias minValue: progressBar.minimumValue
    property real  maxValue: minValue+1
    property alias value:    progressBar.value
    readonly property alias maxValueEff: progressBar.maximumValue
    readonly property real  progress: (value-minValue)/(maxValueEff-minValue)

    function formatProgress(){
        return parseInt(100*progress) + "%"
    }

    // a Mouse area to shield user actions while busy
    MouseArea{
        anchors.fill: parent
    }

    Rectangle{
        id: background
        anchors.fill: parent
        color: colors.currentBackground
        opacity: 0.75
    }

    Column{
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width
        spacing: units.gu(2)

        Label{
            id: lbTitle
            anchors.horizontalCenter: parent.horizontalCenter
        }

        ActivityIndicator{
            anchors.horizontalCenter: parent.horizontalCenter
            running: root.visible
        }

        Label{
            id: lbProgress
            visible: root.showProgress
            anchors.horizontalCenter: parent.horizontalCenter
            text: root.formatProgress()
        }

        ProgressBar{
            id: progressBar
            visible: root.showProgress
            x: units.gu(3)
            width: parent.width - 2*x
            maximumValue: Math.max(minimumValue+1,root.maxValue)
        }
    }
}

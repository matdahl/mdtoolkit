/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

ClippedPanel{
    id: picker
    width:  parent.width
    height: displayedItems * itemHeight

    property real displayedItems: 7
    property int  itemHeight:     units.gu(5)

    property alias model: contentRepeater.model
    property alias delegate: contentRepeater.delegate

    property int selectedIndex: 0
    onSelectedIndexChanged: contentFlickable.contentY = selectedIndex * itemHeight

    readonly property alias contentY: contentFlickable.contentY

    // the Y position of the selected item
    readonly property real selectedY: (displayedItems/2 - 0.5) * itemHeight

    readonly property int firstVisibleIndex: Math.floor((contentY-selectedY)/itemHeight)
    readonly property int lastVisibleIndex:  Math.floor((contentY-selectedY+height)/itemHeight)

    readonly property real opacityStep: 0.5 / (displayedItems/2)

    function itemAt(idx){
        return contentRepeater.itemAt(idx)
    }

    Rectangle{
        id: highlightBackground
        anchors.verticalCenter: parent.verticalCenter
        width:  picker.width
        height: picker.itemHeight
        color:  theme.palette.highlighted.base
        visible: picker.enabled

        Rectangle{
            height: units.gu(0.125)
            width: parent.width
            color: theme.palette.normal.foreground
        }
    }

    Flickable{
        id: contentFlickable
        anchors.fill: parent
        contentHeight: picker.model.count * picker.itemHeight + 2*picker.selectedY

        Behavior on contentY{
            animation: UbuntuNumberAnimation{}
        }

        onMovingChanged: {
            if (!moving){
                picker.selectedIndex = Math.round(contentY/picker.itemHeight)
                contentY = picker.selectedIndex * picker.itemHeight
            }
        }

        Repeater{
            id: contentRepeater
        }
    }
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12

Item{
    id: root
    clip: true

    default property alias contents: contentArea.data

    property alias radius:          background.radius
    property alias backgroundColor: background.color

    Rectangle{
        id: background
        anchors.fill: parent
        radius: units.gu(2)
        color: theme.palette[root.enabled ? "normal" : "disabled"].base
        // layer needs to be enabled to use this item as mask in shader
        layer.enabled: true
    }

    Item{
        id: contentArea
        anchors.fill: parent

        // clip to rounded background rectangle
        layer{
            enabled: true
            samplerName: "content"
            effect: ShaderEffect {
                property var mask: background
                fragmentShader: "
                    uniform lowp sampler2D content;
                    uniform lowp sampler2D mask;
                    uniform lowp float qt_Opacity;
                    varying highp vec2 qt_TexCoord0;
                    void main() {
                        gl_FragColor =
                            texture2D(content, qt_TexCoord0)
                            * texture2D(mask, qt_TexCoord0).a
                            * qt_Opacity;
                    }
                "
            }
        }
    }
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12

Loader{
    id: root
    y: picker.selectedY + row * picker.itemHeight
    width:  picker.width
    height: picker.itemHeight
    readonly property int row: index

    active: index >= picker.firstVisibleIndex &&
            index <= picker.lastVisibleIndex

    default property var contents

    // absolute distance in units of root.itemHeight from the center of the view
    readonly property real distToFocus: Math.abs(y + height/2 - picker.contentY - picker.height/2) / picker.itemHeight

    opacity: 1.0 - distToFocus * picker.opacityStep
    scale: distToFocus < 1.0 ? 1.0 + 0.15*(1.0-distToFocus) : 1.0
    transformOrigin: Item.Left

    sourceComponent: MouseArea{
        id: listItem
        data: root.contents

        onClicked: picker.selectedIndex = row
    }
} // Loader


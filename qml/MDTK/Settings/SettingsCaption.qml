/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Item{
    id: root
    height: units.gu(3)
    width: parent.contentWidth ? parent.contentWidth : parent.width

    property alias title: label.text

    Rectangle{
        anchors.fill: parent
        color: theme.palette.normal.base
        opacity: 0.25
    }

    Label{
        id: label
        anchors.centerIn: parent
    }

    Rectangle{
        anchors.bottom: parent.bottom
        width: root.width
        height: units.gu(0.125)
        color: theme.palette.normal.base
    }
}

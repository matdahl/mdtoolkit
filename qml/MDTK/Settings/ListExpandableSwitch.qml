/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

ListItem {
    id: root
    height: expanded ? expandedHeight : collapsedHeight

    Behavior on height {
        UbuntuNumberAnimation{}
    }

    property alias title:    layout.title
    property alias subtitle: layout.subtitle
    property alias checked:  sw.checked
    property alias actions:  actionsArea.data

    default property alias contents: expandedContent.data

    readonly property int  collapsedHeight: layout.height
    readonly property int  expandedHeight: collapsedHeight + expandedContent.height
    readonly property bool expanded: checked

    ListItemLayout{
        id: layout
        anchors.left: parent.left

        Item{
            id: actionsArea
            SlotsLayout.position: SlotsLayout.Trailing
            height: childrenRect.height
            width: childrenRect.width
        }

        Switch{
            id: sw
            SlotsLayout.position: SlotsLayout.Last
            checked: false
        }
    }

    Item{
        id: expandedContent
        y: root.collapsedHeight
        width: root.width
        height: childrenRect.height
    }
}

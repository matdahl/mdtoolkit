/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3

Expandable {
    id: root
    __contentsMargins: 0

    onClicked: expanded = !expanded
    collapsedHeight: collapsedLayout.height
    expandedHeight: collapsedHeight + expansionColumn.height

    property string title: ""
    property alias  options: repeater.model
    property alias  selectedOption: expansionColumn.selectedIndex

    ListItemLayout{
        id: collapsedLayout
        title.text: root.title
        subtitle.text: repeater.model[expansionColumn.selectedIndex]

        Icon{
            SlotsLayout.position: SlotsLayout.Last
            width: units.gu(3)
            color: theme.palette.normal.backgroundText
            name: root.expanded ? "up" : "down"
        }
    }

    Column{
        id: expansionColumn
        width: root.width
        anchors.top: collapsedLayout.bottom

        readonly property int itemHeight: units.gu(5)

        property int selectedIndex: 0

        Repeater{
            id: repeater
            model: []

            delegate: MouseArea{
                id: listItem
                width:  expansionColumn.width
                height: expansionColumn.itemHeight

                readonly property bool isSelected: expansionColumn.selectedIndex === index

                onClicked: {
                    expansionColumn.selectedIndex = index
                    root.expanded = false
                }

                Rectangle{
                    anchors.fill: parent
                    visible: listItem.isSelected
                    color: theme.palette.selected.background
                }

                Label{
                    anchors{
                        verticalCenter: parent.verticalCenter
                        left: parent.left
                        right: icTick.left
                        leftMargin: units.gu(2)
                        rightMargin: units.gu(1)
                    }
                    color: theme.palette[listItem.isSelected ? "selected" : "normal"].backgroundText
                    text: modelData
                }

                Icon{
                    id: icTick
                    anchors{
                        verticalCenter: parent.verticalCenter
                        right: parent.right
                        rightMargin: units.gu(2)
                    }
                    height: units.gu(2)
                    visible: listItem.isSelected
                    name: "tick"
                    color: theme.palette.selected.backgroundText
                }

                Rectangle{
                    anchors{
                        left: parent.left
                        right: parent.right
                        bottom: parent.bottom
                    }
                    height: units.gu(0.125)
                    color: theme.palette[listItem.isSelected ? "selected" : "normal"].foreground
                }
            }
        }
    }
}

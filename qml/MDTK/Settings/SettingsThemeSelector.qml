/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

SettingsOptionSelector {
    id: root

    // TRANSLATORS: The color theme of the app
    title: i18n.tr("Theme")

    options: [
        // TRANSLATORS: using the color theme of the system
        i18n.tr("System theme"),
        // TRANSLATORS: Suru Dark is the name of the dark color theme
        i18n.tr("Suru Dark (dark)"),
        // TRANSLATORS: Suru Dark is the name of the light color theme
        i18n.tr("Ambiance (light)")
    ]

    selectedOption: {
        if (colors.themeName === "")
            return 0
        if (colors.themeName === "Ubuntu.Components.Themes.Ambiance")
            return 2
        return 1
    }

    onSelectedOptionChanged: {
        switch(selectedOption){
        case 0:
            colors.themeName = ""
            return
        case 1:
            colors.themeName = "Ubuntu.Components.Themes.SuruDark"
            return
        case 2:
            colors.themeName = "Ubuntu.Components.Themes.Ambiance"
        }
    }
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3

Expandable {
    id: root
    __contentsMargins: 0

    onClicked: expanded = !expanded
    collapsedHeight: collapsedLayout.height
    expandedHeight: collapsedHeight + expansionRow.height

    property int buttonMinWidth: units.gu(6)
    readonly property int buttonPadding: units.gu(0.5)
    readonly property int buttonContentRadius: units.gu(0.5)
    readonly property int buttonHeaderHeight: units.gu(1.5)

    readonly property real buttonWidth: Math.max(buttonMinWidth,(root.width-(colors.numColors-1)*expansionRow.width)/colors.numColors)

    ListItemLayout{
        id: collapsedLayout
        // TRANSLATORS: The emphasising color for the app's theme
        title.text: i18n.tr("Accent color")

        Rectangle{
            SlotsLayout.position: SlotsLayout.Leading
            height: units.gu(4)
            width: units.gu(4)
            radius: units.gu(1)
            color: colors.currentHeader
            border{
                width: units.gu(0.5)
                color: theme.palette.normal.base
            }
        }

        Icon{
            SlotsLayout.position: SlotsLayout.Trailing
            width: units.gu(3)
            color: theme.palette[enabled ? "normal" : "disabled"].backgroundText
            name: "reset"
            enabled: colors.currentIndex !== colors.defaultIndex

            MouseArea{
                anchors.fill: parent
                anchors.margins: -units.gu(1)
                onClicked: colors.currentIndex = colors.defaultIndex
                enabled: parent.enabled
            }
        }

        Icon{
            SlotsLayout.position: SlotsLayout.Last
            width: units.gu(3)
            color: theme.palette.normal.backgroundText
            name: root.expanded ? "up" : "down"
        }
    }

    Flickable{
        id: expansionFlickable
        anchors.top: collapsedLayout.bottom
        width: root.width
        height: expansionRow.height
        contentWidth: Math.max(width,expansionRow.width)

        Row {
            id: expansionRow
            height: units.gu(8)
            leftPadding: units.gu(2)
            rightPadding: units.gu(2)
            topPadding: units.gu(1)
            bottomPadding: units.gu(1)
            spacing: units.gu(0.5)
            readonly property int contentHeight: height - topPadding - bottomPadding

            Repeater{
                id: repeater
                model: colors.numColors
                delegate: Button{
                    width: root.buttonWidth
                    height: expansionRow.contentHeight
                    color: theme.palette[colors.currentIndex === index ? "highlighted" : "normal"].base

                    onClicked: {
                        colors.currentIndex = index
                        root.expanded = false
                    }

                    Rectangle{
                        anchors{
                            fill: parent
                            margins: root.buttonPadding
                        }
                        radius: root.buttonContentRadius
                        color: colors.backgroundColors[index][colors.themeIndex]
                    }

                    Item{
                        id: buttonHeaderArea
                        anchors{
                            top: parent.top
                            left: parent.left
                            right: parent.right
                            margins: root.buttonPadding
                        }
                        height: root.buttonHeaderHeight
                        clip: true

                        Rectangle{
                            anchors{
                                fill: parent
                                bottomMargin: -radius
                            }
                            radius: root.buttonContentRadius
                            color: colors.headerColors[index][colors.themeIndex]
                        }
                    }
                }
            }
        }
    }
}

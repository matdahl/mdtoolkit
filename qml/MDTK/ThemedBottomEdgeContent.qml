/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * timetracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Rectangle {
    id: root
    color: colors.currentBackground

    property alias title: header.title
    property alias headerTrailingActionBar: header.trailingActionBar
    property alias headerLeadingActionBar:  header.leadingActionBar
    property alias headerHeight:            header.height

    default property alias contents: column.contents
    property alias headerIcon: headerIconArea.data
    property alias head:       headerArea.data
    property alias footer:     footerArea.data

    property int headPadding:   0
    property int footerPadding: 0

    property alias contentY: column.contentY

    PageHeader{
        id: header
        StyleHints{backgroundColor: colors.currentHeader}

        Item{
            id: headerIconArea
            anchors{
                top: parent.top
                right: parent.right
                bottom: parent.bottom
                margins: units.gu(1)
            }
            width: height
        }
    }

    FlickableColumn{
        id: column
        anchors{
            fill: parent
            topMargin: header.height
        }
        clip: true
        padding: units.gu(2)
        spacing: units.gu(2)
        topFlickMargin:    headerArea.height
        bottomFlickMargin: footerArea.height
    }

    Item{
        id: headerArea
        anchors{
            top: header.bottom
            left: parent.left
            right: parent.right
            margins: root.headPadding
        }
        height: childrenRect.height
    }

    Item{
        id: footerArea
        anchors{
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: root.footerPadding
        }
        height: childrenRect.height
    }
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import MDTK.Exports 1.0 as MDTKExports
import MDTK.Dialogs 1.0 as MDTKDialogs

Item {
    id: root

    signal accepted(string delimiter)
    signal cancelled()

    property string title: i18n.tr("Import CSV file")
    property string acceptText: i18n.tr("Import")
    property string cancelText: i18n.tr("Cancel")

    function open(){
        PopupUtils.open(dialogComponent)
    }

    Component{
        id: dialogComponent
        Dialog{
            id: dialog
            title: root.title

            MDTKExports.DelimiterSelector{
                id: inputDelimiter
            }

            MDTKDialogs.DialogButtons{
                id: buttons
                cancelText: root.cancelText
                acceptText: root.acceptText
                onCancelled: {
                    root.cancelled()
                    PopupUtils.close(dialog)
                }
                onAccepted: {
                    root.accepted(inputDelimiter.selectedDelimiter)
                    PopupUtils.close(dialog)
                }
            }

        }
    }
}

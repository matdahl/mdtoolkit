/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Layouts 1.12 as QQL
import Qt.labs.settings 1.1
import Ubuntu.Components 1.3

ThemedPage{
    id: root

    default property alias tabs: stack.data

    property alias tabHeight: tabHeader.height
    property int   separatorHeight: units.gu(0.25)

    property real tabPadding: 0

    property alias selectedIndex: stack.currentIndex
    property int initialIndex: 0

    onWidthChanged: updateTabPadding()
    function updateTabPadding(){
        var totalImplicitWidth = 0
        var tabCount = 0
        for (var i=0; i<tabRow.children.length; i++){
            // only count visible childrens with an implicitWidth > 0
            if (tabRow.children[i].visible && tabRow.children[i].implicitWidth > 0){
                totalImplicitWidth += tabRow.children[i].implicitWidth
                tabCount++
            }
        }
        const freeSpace = width - totalImplicitWidth
        tabPadding = tabCount > 0 ? Math.max(0,freeSpace/tabCount) : 0
    }

    Settings{
        id: tabViewSettings
        category: "TabView"
        property alias selectedIndex: root.selectedIndex
    }

    Item{
        id: tabHeader
        anchors.top: header.bottom
        height: units.gu(5)
        width: root.width

        Rectangle{
            anchors.fill: parent
            color: colors.currentHeader
            opacity: 0.2
        }

        ScrollView{
            anchors.fill: parent
            horizontalScrollbar.align: Qt.AlignTop

            Row{
                id: tabRow
                height: root.tabHeight

                Repeater{
                    id: tabRepeater
                    model: stack.children.length
                    delegate: TabHeader{
                        id: tab
                        height: root.tabHeight
                        width: implicitWidth + root.tabPadding
                        readonly property var  item: stack.children[index]
                        visible: item.enabled
                        implicitWidth: tabTitle.width + units.gu(2)
                        separatorHeight: root.separatorHeight
                        isSelected: root.selectedIndex === index

                        onClicked: root.selectedIndex = index
                        onVisibleChanged: root.updateTabPadding()
                        onImplicitWidthChanged: root.updateTabPadding()

                        Label{
                            id: tabTitle
                            anchors.centerIn: parent
                            color: tab.isSelected ? theme.palette.normal.backgroundText
                                                  : theme.palette.normal.backgroundTertiaryText
                            font.bold: true
                            text: tab.item.title
                        }
                    }
                }
            }
        }
    }

    QQL.StackLayout{
        id: stack
        anchors{
            top: tabHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        clip: true
        currentIndex: root.initialIndex
    }
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Button{
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width

    property color selectedColor: "#000000"

    onClicked: PopupUtils.open(popoverComponent,this)

    Rectangle{
        anchors{
            fill: parent
            margins: units.gu(0.5)
        }
        radius: units.gu(0.5)
        color: root.selectedColor
        opacity: root.enabled ? 1.0 : 0.5
    }

    Component{
        id: popoverComponent
        Popover{
            id: popover

            property bool initialised: false

            property real h: 0.
            property real s: 0.
            property real l: 0.
            property alias a: sliderAlpha.value

            readonly property color color: Qt.hsla(popover.h,popover.s,popover.l,popover.a)

            Component.onCompleted: {
                h = root.selectedColor.hslHue
                s = root.selectedColor.hslSaturation
                l = root.selectedColor.hslLightness
                a = root.selectedColor.a
                initialised = true
            }

            onColorChanged: {
                if (initialised)
                    root.selectedColor = color
            }

            Column{
                width: parent.width
                padding: units.gu(2)
                spacing: units.gu(2)
                readonly property int contentWidth: width - 2*padding

                Label{
                    width: parent.contentWidth
                    horizontalAlignment: Label.AlignHCenter
                    textSize: Label.Large
                    text: i18n.tr("Select color")
                }

                Rectangle{
                    id: preview
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.contentWidth - units.gu(4)
                    height: units.gu(6)
                    radius: units.gu(1)
                    color: "transparent"
                    border{
                        color: theme.palette.normal.base
                        width: units.gu(0.25)
                    }

                    ColorTransparentBackground{
                        anchors{
                            fill: parent
                            margins: parent.border.width
                        }
                        radius: parent.radius - parent.border.width
                    }

                    Rectangle{
                        anchors{
                            fill: parent
                            margins: parent.border.width
                        }
                        radius: parent.radius - parent.border.width
                        color: popover.color
                    }
                }


                Rectangle{
                    id: hueSelector
                    width: parent.contentWidth
                    height: units.gu(5)
                    radius: units.gu(1)

                    readonly property real gradOffset: units.gu(2)/width
                    readonly property int  numStops:  12+1
                    readonly property real stopDist: (1.0 - 2*gradOffset)/(numStops - 1)

                    gradient: Gradient{
                        id: grad
                        orientation: Gradient.Horizontal

                        function col(hue){
                            return Qt.hsla(hue,1.0,0.5,1.0)
                        }

                        stops: [
                            GradientStop{position: hueSelector.gradOffset;                          color: grad.col( 0.0/12.)},
                            GradientStop{position: hueSelector.gradOffset+ 1.*hueSelector.stopDist; color: grad.col( 1.0/12.)},
                            GradientStop{position: hueSelector.gradOffset+ 2.*hueSelector.stopDist; color: grad.col( 2.0/12.)},
                            GradientStop{position: hueSelector.gradOffset+ 3.*hueSelector.stopDist; color: grad.col( 3.0/12.)},
                            GradientStop{position: hueSelector.gradOffset+ 4.*hueSelector.stopDist; color: grad.col( 4.0/12.)},
                            GradientStop{position: hueSelector.gradOffset+ 5.*hueSelector.stopDist; color: grad.col( 5.0/12.)},
                            GradientStop{position: hueSelector.gradOffset+ 6.*hueSelector.stopDist; color: grad.col( 6.0/12.)},
                            GradientStop{position: hueSelector.gradOffset+ 7.*hueSelector.stopDist; color: grad.col( 7.0/12.)},
                            GradientStop{position: hueSelector.gradOffset+ 8.*hueSelector.stopDist; color: grad.col( 8.0/12.)},
                            GradientStop{position: hueSelector.gradOffset+ 9.*hueSelector.stopDist; color: grad.col( 9.0/12.)},
                            GradientStop{position: hueSelector.gradOffset+10.*hueSelector.stopDist; color: grad.col(10.0/12.)},
                            GradientStop{position: hueSelector.gradOffset+11.*hueSelector.stopDist; color: grad.col(11.0/12.)},
                            GradientStop{position: hueSelector.gradOffset+12.*hueSelector.stopDist; color: grad.col(12.0/12.)}
                        ]
                    }

                    Slider{
                        anchors.centerIn: parent
                        width: parent.width - units.gu(4)
                        minimumValue: 0.0
                        maximumValue: 1.0
                        value: popover.h
                        onValueChanged: popover.h = value
                        function formatValue(v){
                            return v.toFixed(2)
                        }
                    }
                }

                Rectangle{
                    id: slSelector
                    width: parent.contentWidth
                    height: units.gu(24)
                    color: theme.palette.normal.base

                    ShaderEffect{
                        anchors{
                            fill: parent
                            margins: units.gu(0.25)
                        }

                        readonly property real h: popover.h

                        fragmentShader: "
                            uniform mediump float h;
                            uniform mediump float qt_Opacity;
                            varying highp   vec2  qt_TexCoord0;

                            mediump float f(mediump float n,mediump float s,mediump float l) {
                                mediump float k = mod(n+12.*h,12.);
                                mediump float a = s*min(l,1.0-l);
                                return l - a *max(-1.,min(k-3.,min(9.-k,1.)));
                            }

                            void main() {
                                mediump float s = qt_TexCoord0.x;
                                mediump float l = 1.0 - qt_TexCoord0.y;

                                gl_FragColor = vec4(
                                        f(0.,s,l),
                                        f(8.,s,l),
                                        f(4.,s,l),
                                        1.0)
                                     * qt_Opacity;
                            }
                        "
                    }

                    Rectangle{
                        id: slCursor
                        x: popover.s * parent.width - size/2
                        y: (1. - popover.l) * parent.height- size/2
                        height: size
                        width:  size
                        radius: size/2
                        readonly property int size: units.gu(2)

                        color: "transparent"
                        border{
                            width: units.gu(0.5)
                            color: theme.palette.normal.selection
                        }
                        Rectangle{
                            anchors.fill: parent
                            radius: parent.radius
                            color: "transparent"
                            border{
                                width: units.gu(0.125)
                                color: theme.palette.normal.base
                            }
                        }
                    }

                    MouseArea{
                        anchors.fill: parent
                        onPressed: {
                            popover.s = mouse.x / slSelector.width
                            popover.l = 1.0 - mouse.y / slSelector.height
                        }
                        hoverEnabled: pressed
                        onMouseXChanged: popover.s = mouse.x / slSelector.width
                        onMouseYChanged: popover.l = 1.0 - mouse.y / slSelector.height
                    }
                }

                Rectangle{
                    id: alphaSelector
                    width: parent.contentWidth
                    height: units.gu(5)
                    radius: units.gu(1)

                    readonly property real gradOffset: units.gu(2)/width

                    gradient: Gradient{
                        orientation: Gradient.Horizontal

                        stops: [
                            GradientStop{position:       hueSelector.gradOffset; color: "transparent"},
                            GradientStop{position: 1.0 - hueSelector.gradOffset; color: popover.color}
                        ]
                    }

                    Slider{
                        id: sliderAlpha
                        anchors.centerIn: parent
                        width: parent.width - units.gu(4)
                        minimumValue: 0.0
                        maximumValue: 1.0
                        live: true
                        function formatValue(v){
                            return (100*v).toFixed(0) + "%"
                        }
                    }
                }
            }
        }
    }
}

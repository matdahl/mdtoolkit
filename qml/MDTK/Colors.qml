/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components.Themes 1.3
import Qt.labs.settings 1.1

Item {
    id: root

    Settings{
        id: settings
        category: "Theming"
        property alias colorIndex: root.currentIndex
        property alias themeName:  root.themeName
    }

    // the name of the currently selected theme - if empty system theme will be used
    property string themeName: ""

    // the index of default color flavor
    property int defaultIndex: 0

    // the currently selected color scheme
    property int currentIndex: defaultIndex
    onCurrentIndexChanged: {
        if (currentIndex<0 || currentIndex >= numColors)
            currentIndex = 0
    }

    // the current colors
    readonly property int   themeIndex: theme.name === "Ubuntu.Components.Themes.Ambiance" ? 1 : 0
    readonly property bool  darkMode:   themeIndex === 0
    readonly property color currentHeader:     headerColors    [currentIndex][themeIndex]
    readonly property color currentBackground: backgroundColors[currentIndex][themeIndex]

    readonly property int numColors: headerColors.length
    readonly property var headerColors:
        [["#111111","#ffffff"], // theme background colors
         ["#960334","#FB3778"], // 'magenta' gpick: hue: 340, saturation:  96%, lightness -20%/+10%
         ["#7C0396","#D937FB"], // 'purple'  gpick: hue: 290, saturation:  96%, lightness -20%/+10%
         ["#030396","#3737FB"], // 'blue'    gpick: hue: 240, saturation:  96%, lightness -20%/+10%
         ["#008585","#33FFFF"], // 'cyan'    gpick: hue: 180, saturation: 100%, lightness -24%/+10%
         ["#038203","#37FB37"], // 'green'   gpick: hue: 120, saturation:  96%, lightness -24%/+10%
         ["#787802","#FBFB37"], // 'yellow'  gpick: hue:  60, saturation:  96%, lightness -26%/+10%
         ["#964D03","#FB9937"]  // 'orange'  gpick: hue:  30, saturation:  96%, lightness -20%/+10%
        ]
    readonly property var backgroundColors:
        [["#111111","#ffffff"], // theme background colors
         ["#1E010A","#FEE1EB"], // 'magenta' gpick: hue: 340, saturation:  96%, lightness -44%/+44%
         ["#19011E","#F9E1FE"], // 'purple'  gpick: hue: 290, saturation:  96%, lightness -44%/+44%
         ["#01011E","#E1E1FE"], // 'blue'    gpick: hue: 240, saturation:  96%, lightness -44%/+44%
         ["#001F1F","#E0FFFF"], // 'cyan'    gpick: hue: 180, saturation: 100%, lightness -44%/+44%
         ["#011E01","#E1FEE1"], // 'green'   gpick: hue: 120, saturation:  96%, lightness -44%/+44%
         ["#1E1E01","#FEFEE1"], // 'yellow'  gpick: hue:  60, saturation:  96%, lightness -44%/+44%
         ["#1E0F01","#FEF0E1"]  // 'orange'  gpick: hue:  30, saturation:  96%, lightness -44%/+44%
        ]

    readonly property color yellow: Qt.hsla(0.165,1,themeIndex === 0 ? 0.65 : 0.45 )
}


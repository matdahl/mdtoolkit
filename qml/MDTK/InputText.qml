/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Column{
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width
    height: textField.y + textField.height
    spacing: units.gu(2)

    property alias title:     label.text
    property bool  checkable: false
    property alias checked:   sw.checked

    property alias text: textField.text
    property alias placeholderText: textField.placeholderText
    readonly property alias displayText: textField.displayText

    Item{
        id: labelRow
        width: parent.width
        height: label.height
        visible: label.text.length > 0

        Label{
            id: label
            anchors{
                left:  parent.left
                right: sw.visible ? sw.left : parent.right
                rightMargin: sw.visible ? units.gu(1) : 0
            }
            enabled: !root.checkable || root.checked
            wrapMode: Label.Wrap
        }

        Switch{
            id: sw
            anchors{
                right: parent.right
                verticalCenter: parent.verticalCenter
            }
            visible: root.checkable
        }
    }

    TextField{
        id: textField
        width: root.width
        enabled: !root.checkable || root.checked
    }
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12

ThemedPage{
    id: root

    default property alias contents: column.data
    property alias head:   headerArea.data
    property alias footer: footerArea.data

    property int headPadding:   0
    property int footerPadding: 0

    property alias padding: column.padding
    property alias spacing: column.spacing
    readonly property int contentWidth: width - 2*padding

    Flickable{
        id: flickable
        anchors{
            top: headerArea.bottom
            left: parent.left
            right: parent.right
            bottom: footerArea.top
        }
        contentHeight: Math.max(height,column.height)

        Column{
            id: column
            width: flickable.width
            readonly property int contentWidth: width - 2*padding
        }
    }

    Item{
        id: headerArea
        anchors{
            top: header.bottom
            left: parent.left
            right: parent.right
            margins: root.headPadding
        }
        height: childrenRect.height
    }

    Item{
        id: footerArea
        anchors{
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: root.footerPadding
        }
        height: childrenRect.height
    }
}

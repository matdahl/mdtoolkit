/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.Pickers 1.3

Button {
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width

    property date month: new Date()

    property date minMonth: new Date(new Date().getFullYear()-20,1,1)
    property date maxMonth: new Date(new Date().getFullYear()+30,1,1)

    onClicked: PopupUtils.open(popoverComponent,this)

    property string format: "MMMM yyyy"
    property alias _iconName:   icon.name
    property alias _iconSource: icon.source
    property alias _iconColor:  icon.color

    property string enabledText:  defaultEnabledText
    property string disabledText: defaultDisabledText
    readonly property string defaultEnabledText:  Qt.formatDate(month,format)
    readonly property string defaultDisabledText: "-"

    Icon{
        id: icon
        anchors.verticalCenter: parent.verticalCenter
        x: units.gu(1)
        height: 0.5*parent.height
        color: label.color
    }

    Label{
        id: label
        anchors{
            verticalCenter: parent.verticalCenter
            left: icon.left
            right: parent.right
        }
        horizontalAlignment: Label.AlignHCenter
        text: enabled ? root.enabledText : root.disabledText
        enabled: root.enabled
    }

    Component{
        id: popoverComponent
        Popover{
            id: popover
            contentHeight: picker.height + 2*picker.y

            // block dateChanged signals during init phase
            property bool initialised: false
            Component.onCompleted: initialised = true

            DatePicker{
                id: picker
                y: units.gu(2)
                anchors.horizontalCenter: parent.horizontalCenter
                date: root.month
                minimum: root.minMonth
                maximum: root.maxMonth
                mode: "Years|Months"
                onDateChanged: {
                    if (popover.initialised)
                        root.month = date
                }
            }
        }
    }
}

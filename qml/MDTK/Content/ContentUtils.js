/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

.import Lomiri.Content 1.3 as LomContent

function transferDirectionToString(direction){
    switch(direction){
    case LomContent.ContentTransfer.Import:
        return "Import"
    case LomContent.ContentTransfer.Export:
        return "Export"
    case LomContent.ContentTransfer.Share:
        return "Share"
    }
    return "UNKNOWN"
}

function transferSelectionTypeToString(type){
    switch(type){
    case LomContent.ContentTransfer.Single:
        return "Single"
    case LomContent.ContentTransfer.Multiple:
        return "Multiple"
    }
    return "UNKNOWN"
}

function transferContentTypeToString(type){
    switch(type){
    case LomContent.ContentType.Unknown:
        return "Unknown"
    case LomContent.ContentType.Documents:
        return "Documents"
    case LomContent.ContentType.Pictures:
        return "Pictures"
    case LomContent.ContentType.Music:
        return "Music"
    case LomContent.ContentType.Contacts:
        return "Contacts"
    case LomContent.ContentType.Videos:
        return "Videos"
    case LomContent.ContentType.Links:
        return "Links"
    case LomContent.ContentType.EBooks:
        return "EBooks"
    case LomContent.ContentType.Text:
        return "Text"
    case LomContent.ContentType.All:
        return "All"
    }
    return "UNKNOWN"
}


function printTransferDetails(transfer){
    console.info("Content transfer:")
    console.info("    source:         ",transfer.source)
    console.info("    destination:    ",transfer.destination)
    console.info("    direction:      ",transferDirectionToString(transfer.direction))
    console.info("    content type:   ",transferContentTypeToString(transfer.contentType))
    console.info("    selection type: ",transferSelectionTypeToString(transfer.selectionType))
    console.info("    number of items:",transfer.items.length)
}


import QtQuick 2.12
import Ubuntu.Components 1.3

Item {
    id: root

    width: units.gu(16)
    height: units.gu(4)

    property int value: 0
    property int minValue: -10
    property int maxValue:  10

    onMinValueChanged: {
        if (value < minValue)
            value = minValue
    }
    onMaxValueChanged: {
        if (value > maxValue)
            value = maxValue
    }

    property bool showSlider: true

    Rectangle{
        id: background
        anchors.fill: parent
        radius: units.gu(1)
        color: "#33333333"
    }


    Rectangle{
        anchors{
            top: parent.top
            left: parent.left
            right: parent.horizontalCenter
            bottom: parent.bottom
        }
        radius: units.gu(1)
        opacity: 0.4
        gradient: Gradient{
            orientation: Gradient.Horizontal
            GradientStop{
                position: 1.0*root.height/root.width
                color: theme.palette.normal.negative
            }
            GradientStop{
                position: 3.5*root.height/root.width
                color: "#00000000"
            }
        }
    }

    Rectangle{
        anchors{
            top: parent.top
            left: parent.horizontalCenter
            right: parent.right
            bottom: parent.bottom
        }
        radius: units.gu(1)
        opacity: 0.4
        gradient: Gradient{
            orientation: Gradient.Horizontal
            GradientStop{
                position: 1 - 1.0*root.height/root.width
                color: theme.palette.normal.positive
            }
            GradientStop{
                position: 1 - 3.5*root.height/root.width
                color: "#00000000"
            }
        }
    }

    Rectangle{
        id: border
        anchors.fill: parent
        radius: units.gu(1)
        color: "#00000000"
        border{
            width: units.gu(0.125)
            color: theme.palette.normal.base
        }
    }

    Rectangle{
        anchors{
            top: parent.top
            right: parent.left
            bottom: parent.bottom
            margins: units.gu(0.5)
            rightMargin: -parent.height
        }
        width: units.gu(0.125)
        color: theme.palette.normal.base
    }

    Rectangle{
        anchors{
            top: parent.top
            left: parent.right
            bottom: parent.bottom
            margins: units.gu(0.5)
            leftMargin: -parent.height
        }
        width: units.gu(0.125)
        color: theme.palette.normal.base
    }

    Item{
        id: sliderArea
        anchors{
            top: parent.top
            left: mouseDec.right
            right: mouseInc.left
            bottom: parent.bottom
        }
        visible: root.showSlider

        readonly property real progress: (root.value - root.minValue) / (root.maxValue - root.minValue)

        Rectangle{
            id: slider
            anchors.verticalCenter: sliderArea.verticalCenter
            height: sliderArea.height - units.gu(1)
            width: units.gu(1.5)
            radius: units.gu(0.75)
            color: theme.palette.normal.base
            opacity: 0.8
            x: sliderArea.progress * (sliderArea.width - width)
        }

        MouseArea{
            anchors.fill: parent
            onPositionChanged: {
                var relX = (mouse.x-slider.width/2)/(sliderArea.width-slider.width)
                if (relX < 0)
                    relX = 0
                else if (relX > 1)
                    relX = 1
                root.value = minValue + Math.round(relX*(maxValue-minValue))
            }
        }
    }

    Label{
        id: lbValue
        anchors.centerIn: parent
        text: root.value
        textSize: Label.Large
        font.bold: true
    }

    MouseArea{
        id: mouseDec
        anchors{
            top: parent.top
            left: parent.left
            bottom: parent.bottom
        }
        width: height
        enabled: root.value > root.minValue
        onClicked: root.value -= 1
        Label{
            anchors.centerIn: parent
            text: "-"
            textSize: Label.Large
            font.bold: true
        }
    }

    MouseArea{
        id: mouseInc
        anchors{
            top: parent.top
            right: parent.right
            bottom: parent.bottom
        }
        width: height
        enabled: root.value < root.maxValue
        onClicked: root.value += 1
        Label{
            anchors.centerIn: parent
            text: "+"
            textSize: Label.Large
            font.bold: true
        }
    }
}

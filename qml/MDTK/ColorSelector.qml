/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Flickable {
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width
    height: rows*size + (rows-1)*minSpacing
    contentWidth: Math.max(width,content.width)

    // expects a model that provides the role modelColor
    property alias model: repeater.model

    // the selected color index
    property int selectedIndex: -1

    // optionaly, the usage counter can be decrease by 1 for a color
    property int decCountsForColor: -1

    // the direction how colors are arranged: left-right or top-down
    property bool horizontal: true

    // size of the color buttons
    property int size: units.gu(5)

    // minimal spacing between color buttons
    property int minSpacing: units.gu(0.5)

    property int cols: Math.floor((width+minSpacing)/(size+minSpacing))
    readonly property int rows: Math.ceil(model.count/cols)
    readonly property real effSpacing: Math.max((width-cols*size)/(cols-1),minSpacing)

    Component.onCompleted: {
        // center selectedIndex if needed
        if (contentWidth > width && selectedIndex > 0){
            const selectedCol = horizontal ? selectedIndex%cols : Math.floor(selectedIndex/rows)
            const selectedXCenter = selectedCol*(size+effSpacing) + size/2
            contentX = Math.max(0,Math.min(contentWidth-width,selectedXCenter-width/2))
        }
    }

    Item{
        id: content
        width: childrenRect.width
        height: root.height

        Repeater{
            id: repeater
            delegate: Item{
                id: delegateItem
                readonly property int col: root.horizontal ? index%root.cols             : Math.floor(index/root.rows)
                readonly property int row: root.horizontal ? Math.floor(index/root.cols) : index%root.rows

                readonly property bool isSelected: root.selectedIndex === index
                readonly property int  effUsage: (root.decCountsForColor === index ? modelCounts-1 : modelCounts)

                x: col*(root.size+root.effSpacing) - (isSelected ? root.minSpacing : 0)
                y: row*(root.size+root.minSpacing) - (isSelected ? root.minSpacing : 0)
                width: root.size + (isSelected ? 2*root.minSpacing : 0)
                height: width

                Button{
                    anchors.fill: parent
                    color: theme.palette[delegateItem.isSelected ? "selected" : "normal"].foreground
                    opacity: isSelected ? 1.0 : 0.5

                    Rectangle{
                        anchors.centerIn: parent
                        width: 0.75*parent.width
                        height: 0.75*parent.height
                        radius: units.gu(0.5)
                        color: modelColor
                    }

                    onClicked: root.selectedIndex = index
                }

                Rectangle{
                    anchors{
                        right: parent.right
                        bottom: parent.bottom
                    }
                    height: units.gu(2.5)
                    width: height
                    radius: height/2
                    visible: delegateItem.effUsage > 0
                    color: theme.palette[delegateItem.isSelected ? "selected" : "normal"].negative

                    border{
                        width: units.gu(0.125)
                        color: theme.palette.normal.base
                    }

                    Label{
                        anchors.centerIn: parent
                        color: theme.palette[delegateItem.isSelected ? "selected" : "normal"].negativeText
                        text: delegateItem.effUsage
                    }
                }
            }
        }
    }
}

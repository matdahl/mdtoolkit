/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * finance is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12

Item {
    id: root

    width: units.gu(5)
    height: width
    readonly property int size: height < width ? height : width

    /*
      The model must provide a property *sum* and the roles
      - color modelColor
      - real  modelValue
      - real  modelPriorValues
     */
    property alias model: rep.model

    property alias emptyColor: emptyPie.color

    Rectangle{
        id: emptyPie
        anchors.centerIn: parent
        height: root.size
        width:  root.size
        radius: root.size/2
        visible: model.count < 1
        color: theme.palette.normal.base
    }

    Item{
        id: pie
        anchors.centerIn: parent
        height: root.size
        width:  root.size
        readonly property int radius: Math.max(root.size/2,1)
        visible: model.count > 0

        Repeater{
            id: rep
            delegate: Canvas {
                id: slice
                anchors.fill: parent

                readonly property color color: modelColor
                readonly property int   r: pie.radius
                readonly property real  startAngle: (modelPriorValues/rep.model.sum - 0.25) * Math.PI * 2
                readonly property real  endAngle:   ((modelPriorValues+modelValue)/rep.model.sum - 0.25) * Math.PI * 2

                onColorChanged: requestPaint()
                onRChanged: requestPaint()
                onStartAngleChanged: requestPaint()
                onEndAngleChanged: requestPaint()

                onPaint: {
                    var ctx = getContext("2d");
                    ctx.reset();

                    ctx.beginPath();
                    ctx.fillStyle = slice.color;
                    ctx.moveTo(slice.r, slice.r);
                    ctx.arc(slice.r, slice.r, slice.r, slice.startAngle, slice.endAngle, false);
                    ctx.lineTo(slice.r, slice.r);
                    ctx.fill();
                }
            }
        }
    }
}

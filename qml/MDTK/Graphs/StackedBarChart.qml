/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * finance is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12

Item {
    id: root

    width: units.gu(5)
    height: units.gu(5)

    property bool isHorizontal: true

    /*
      The model must provide a property *sum* and the roles
      - color modelColor
      - real  modelValue
     */
    property var model

    readonly property int  pxlTotal: isHorizontal ? width : height
    readonly property real pxlPerUnit: pxlTotal / model.sum

    Rectangle{
        id: emptyBar
        anchors.fill: parent
        visible: model.count < 1
        color: theme.palette.normal.base
    }

    Row{
        height: root.height
        visible: root.isHorizontal
        Repeater{
            model: root.model
            delegate: Rectangle{
                height: root.height
                width:  modelValue * root.pxlPerUnit
                color: modelColor
            }
        }
    }

    Column{
        width: root.width
        visible: !root.isHorizontal
        Repeater{
            model: root.model
            delegate: Rectangle{
                height: modelValue * root.pxlPerUnit
                width:  root.width
                color: modelColor
            }
        }
    }
}

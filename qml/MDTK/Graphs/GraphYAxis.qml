/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * finance is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Item {
    id: root
    width: labelMaxWidth + spacing + ticksWidth

    property bool stickToZero: true
    property real min: 0
    property real max: 1
    property real minEff: min
    property real maxEff: max

    readonly property real pxlPerUnit: height/(maxEff-minEff)
    readonly property real zeroY: maxEff*pxlPerUnit

    property int  minTicks: 3
    property int  maxTicks: Math.max(height/units.gu(2),6)
    property real ticksInterval: 1
    property int  ticksNumber: 2
    property int  labelMaxWidth: 0
    property int  spacing: units.gu(0.5)
    property string unitSymbol: ""

    property QtObject grid: QtObject {
        property int   width:     parent.gridWidth ? parent.gridWidth : units.gu(0.125)
        property int   axisWidth: parent.axisWidth ? parent.axisWidth : units.gu(0.25)
        property color color:     parent.gridColor ? parent.gridColor : theme.palette.normal.base
    }

    property int ticksLength: parent.ticksLength ? parent.ticksLength : units.gu(0.5)
    property int ticksWidth:  parent.ticksWidth  ? parent.ticksWidth  : units.gu(0.25)
    property int graphWidth:  parent.graphArea  ? parent.graphArea.width : units.gu(10)

    onMinChanged: updateYAxis()
    onMaxChanged: updateYAxis()
    function updateYAxis(){
        // only continue if both min and max are not NaN
        if (min !== min || max !== max || min >= max)
            return

        const realMax = stickToZero ? Math.max(max,0) : max
        const realMin = stickToZero ? Math.min(min,0) : min
        const invervalLength = realMax - realMin

        var tickInterval = [1,1]
        var tickIntervalValue = tickInterval[0]*tickInterval[1]

        var positiveTicks = realMax > 0 ?  Math.ceil(realMax/tickIntervalValue)  : 0
        var negativeTicks = realMin < 0 ? -Math.floor(realMin/tickIntervalValue) : 0
        var numTicks = 1 + positiveTicks + negativeTicks

        // lower scale if there are too few ticks
        while (numTicks<minTicks){
            if (tickInterval[0] === 5)
                tickInterval[0] = 2
            else if (tickInterval[0] === 2)
                tickInterval[0] = 1
            else {
                tickInterval[0] = 5
                tickInterval[1] /= 10
            }
            tickIntervalValue = tickInterval[0]*tickInterval[1]
            positiveTicks = realMax > 0 ?  Math.ceil(realMax/tickIntervalValue)  : 0
            negativeTicks = realMin < 0 ? -Math.floor(realMin/tickIntervalValue) : 0
            numTicks = 1 + positiveTicks + negativeTicks
        }
        // raise scale if there are too many ticks
        while (numTicks>maxTicks){
            if (tickInterval[0] === 1)
                tickInterval[0] = 2
            else if (tickInterval[0] === 2)
                tickInterval[0] = 5
            else {
                tickInterval[0] = 1
                tickInterval[1] *= 10
            }
            tickIntervalValue = tickInterval[0]*tickInterval[1]
            positiveTicks = realMax > 0 ?  Math.ceil(realMax/tickIntervalValue)  : 0
            negativeTicks = realMin < 0 ? -Math.floor(realMin/tickIntervalValue) : 0
            numTicks = 1 + positiveTicks + negativeTicks
        }

        root.maxEff        = Math.ceil(realMax/tickIntervalValue)*tickIntervalValue
        root.minEff        = Math.floor(realMin/tickIntervalValue)*tickIntervalValue
        root.ticksInterval = tickIntervalValue
        root.ticksNumber   = numTicks
    }

    Rectangle{
        id: axis
        anchors.right: parent.right
        width:  root.grid.axisWidth
        height: parent.height
        color: root.grid.color
    }

    Repeater{
        model: root.ticksNumber
        delegate: Rectangle {
            anchors.right: parent.right
            y: root.height - modelData*root.ticksInterval*root.pxlPerUnit - height/2
            width:  root.ticksLength
            height: root.ticksWidth
            color: root.grid.color

            Rectangle{
                id: horizontalGrid
                anchors{
                    left: parent.right
                    verticalCenter: parent.verticalCenter
                }
                width: root.graphWidth
                height: root.grid.width
                color:  root.grid.color
            }

            Label{
                anchors{
                    right: parent.left
                    rightMargin: root.spacing
                    verticalCenter: parent.verticalCenter
                }
                readonly property real value: root.minEff + modelData*root.ticksInterval
                readonly property int decimal: root.ticksInterval>10 ? 0 : root.ticksInterval>1 ? 1 : 2
                readonly property string symbol: (root.ticksInterval > 1e6-1 ? "M" : root.ticksInterval > 1e3-1 ? "k" : "")+root.unitSymbol
                readonly property real valueEff: (root.ticksInterval > 1e6-1 ? 1e-6 : root.ticksInterval > 1e3-1 ? 1e-3 : 1)*value
                text: valueEff.toLocaleString(Qt.locale(),'f',decimal) +" "+symbol
                textSize: Label.Small
                Component.onCompleted: {
                    if (width > root.labelMaxWidth)
                        root.labelMaxWidth = width
                }
            }
        }
    }
}

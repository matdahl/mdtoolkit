/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12

Item {
    id: root

    default property alias graphContent: graphAreaContent.data

    /*
      Model must provide following properties:
      real maxValue
      int  firstYear
      int  lastYear
     */
    property var model

    property Component yearDelegate

    property int paddingTop:    padding
    property int paddingLeft:   padding
    property int paddingRight:  padding
    property int paddingBottom: padding
    property int padding:       0

    property GraphYAxis yAxis: GraphYAxis{
        parent: root
        y: paddingTop
        x: paddingLeft
        height: graphArea.height
        max: Math.max(root.model.maxValue,1)
    }

    property GraphYearAxis xAxis: GraphYearAxis{
        parent: root
        x: graphArea.x
        y: root.paddingTop
        width: graphArea.width
        height: root.height - y - root.paddingBottom
        minYear: root.model.firstYear
        maxYear: root.model.lastYear
    }

    property Item graphArea: Item{
        parent: root
        x: yAxis.x + yAxis.width
        y: root.paddingTop
        height: root.height - y - root.paddingBottom - xAxis.labelHeight - xAxis.grid.axisWidth
        width:  root.width  - x - root.paddingRight
        clip: true

        Item{
            id: graphAreaContent
            height: parent.height
            x: -xAxis.contentX

            Repeater{
                model: root.model
                delegate: root.yearDelegate
            }
        }
    }
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * finance is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Item {
    id: root
    clip: true

    property int  minYear: new Date().getFullYear()
    property int  maxYear: new Date().getFullYear()
    readonly property int  numYears: maxYear - minYear + 1

    readonly property int labelHeight: labelMaxHeight + spacing + grid.axisWidth
    readonly property int graphAreaHeight: height - labelHeight - grid.axisWidth

    property real minPxlPerUnit: units.gu(4)
    property int  minWidthPerLabel: units.gu(6)
    property int  labelMaxHeight: units.gu(2)
    property int  spacing: units.gu(0.5)

    property QtObject grid: QtObject {
        property int   width:     units.gu(0.125)
        property int   axisWidth: units.gu(0.25)
        property color color:     theme.palette.normal.base
    }

    readonly property real implizitPxlPerUnit: width/numYears
    readonly property real pxlPerUnit: Math.max(minPxlPerUnit,implizitPxlPerUnit)
    readonly property int  labelSkip: Math.floor(minWidthPerLabel/pxlPerUnit)

    property alias contentX: flickable.contentX
    property alias contentWidth: flickable.contentWidth

    Rectangle{
        id: axis
        y: root.graphAreaHeight
        width: parent.width
        height: grid.axisWidth
        color:  grid.color
    }

    Flickable{
        id: flickable
        anchors.fill: parent
        contentWidth: rowGrid.width

        Row{
            id: rowGrid
            height: flickable.height

            Repeater{
                model: root.numYears
                delegate: Item{
                    id: gridItem
                    width: root.pxlPerUnit
                    height: root.height
                    readonly property int year: root.minYear + index
                    readonly property bool isNow:  year === new Date().getFullYear()
                    readonly property bool isPast: year  <  new Date().getFullYear()
                    readonly property bool showLabel: root.labelSkip<1 || (index-root.minYear) % root.labelSkip === 0

                    readonly property date today: new Date(new Date().getFullYear(),6,1);
                    readonly property date firstDay: new Date(root.minYear+modelData,0,1)
                    readonly property date lastDay:  new Date(root.minYear+modelData,11,31)

                    Rectangle{
                        id: timeMarker
                        height: root.graphAreaHeight
                        width:  parent.width
                        visible: gridItem.isNow || gridItem.isPast
                        opacity: gridItem.isPast ? 0.3 : 1
                        color: theme.palette.normal.selection
                    }

                    Rectangle{
                        id: rectGrid
                        height: root.graphAreaHeight
                        width:  parent.width
                        visible: modelData % 2 === 1
                        opacity: 0.2
                        color: theme.palette.normal.base
                    }

                    Rectangle{
                        id: tick
                        anchors.horizontalCenter: parent.horizontalCenter
                        y: root.graphAreaHeight + root.grid.axisWidth
                        width: root.grid.axisWidth
                        height: 0.5*root.spacing
                        color: root.grid.color
                        visible: gridItem.showLabel
                    }

                    Label{
                        anchors.horizontalCenter: parent.horizontalCenter
                        y: root.graphAreaHeight + root.spacing + root.grid.axisWidth
                        visible: gridItem.showLabel
                        text: gridItem.year
                        textSize: Label.Small
                    }
                }
            }
        }
    }
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Column{
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width
    spacing: units.gu(2)

    property alias model: selector.model
    readonly property string selectedDelimiter: selector.model[selector.selectedIndex]

    function selectDelimiter(delim){
        for (var i=0; i<model.length; i++){
            if (model[i] === delim){
                selector.selectedIndex = i
                return
            }
        }
    }

    Label{
        id: label
        text: i18n.tr("Delimiter")
    }

    HorizontalCharacterSelector{
        id: selector
        model: ["\t"," ",";",","]
    }
}

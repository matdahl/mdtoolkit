/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * account-book is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Item {
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width
    height: units.gu(6)

    property alias model: selector.model
    property alias selectedIndex: selector.selectedIndex

    property int itemWidth: units.gu(6)

    Item{
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height
        width: selector.height

        OptionSelector{
            id: selector
            x: height/2 - width/2
            y: - height/2 + width/2
            width: root.height
            rotation: -90
            expanded: true
            containerHeight: Math.min(model.length*root.itemWidth,root.width)
            delegate: Rectangle{
                height: root.itemWidth
                width: selector.width
                color: theme.palette[selector.selectedIndex === index ? "highlighted" : "normal"].background

                Label{
                    anchors.centerIn: parent
                    rotation: 90
                    text: {
                        if (modelData === "BLANCK" || modelData === " ")
                            return "_"
                        if (modelData === "TAB" || modelData === "\t")
                            return "\\t"
                        return modelData
                    }
                }

                MouseArea{
                    anchors.fill: parent
                    onClicked: selector.selectedIndex = index
                }
            }
        }
    }
}

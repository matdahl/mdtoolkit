/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Column{
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width
    spacing: units.gu(2)

    property string defaultFilename: "file"
    property string fileExtension: "csv"
    readonly property string filename: (textField.displayText.length > 0 ? textField.displayText : textField.placeholderText) + "." + fileExtension

    Label{
        text: i18n.tr("Filename")
    }

    Item{
        id: inputRow
        width: root.width
        height: textField.height

        TextField{
            id: textField
            anchors{
                left: parent.left
                right: lbFileExtension.left
                rightMargin: units.gu(1)
            }
            placeholderText: root.defaultFilename
        }
        Label{
            id: lbFileExtension
            anchors{
                verticalCenter: textField.verticalCenter
                right: parent.right
            }
            text: "." + root.fileExtension
        }
    }
}

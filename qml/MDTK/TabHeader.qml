/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12

MouseArea{
    id: root

    property bool isSelected: false
    property int  separatorHeight: units.gu(0.25)

    Rectangle{
        anchors.fill: parent
        visible: root.isSelected
        color: theme.palette.normal.selection
        opacity: 0.7
    }

    Rectangle{
        anchors.bottom: parent.bottom
        height: (root.isSelected ? 1.5 : 1.0) * root.separatorHeight
        width: parent.width
        color: theme.palette.normal[root.isSelected ? "selection" : "base" ]
    }
}

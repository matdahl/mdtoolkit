/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Column {
    id: root
    width: parent.contentWidth
    spacing: units.gu(2)

    property alias showOnStart: cb.checked

    signal closed()

    ManualSeparator{}

    Item{
        width: root.width
        height: Math.max(label.height,cb.height)

        Label{
            id: label
            anchors{
                verticalCenter: parent.verticalCenter
                left: parent.left
                right: cb.left
                rightMargin: units.gu(1)
            }
            text: i18n.tr("Show manual on start")
            wrapMode: Label.Wrap
        }

        CheckBox{
            id: cb
            anchors{
                verticalCenter: parent.verticalCenter
                right: parent.right
            }
            checked: true
        }
    }

    Button{
        id: btClose
        anchors.horizontalCenter: parent.horizontalCenter
        width: 0.6*root.width
        color: UbuntuColors.orange
        text: i18n.tr("Close")
        onClicked: root.closed()
    }
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Column {
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width

    property alias model: repeater.model

    property int   indent: units.gu(2)
    property int   bulletPointSpacing: units.gu(1)
    property int   bulletPointHeight: units.gu(1)
    property color bulletPointColor: theme.palette.normal.base

    Repeater{
        id: repeater
        delegate: Item{
            width: root.width
            height: label.height

            Rectangle{
                id: bulletPoint
                x: root.indent
                y: (units.gu(2)-height)/2
                height: root.bulletPointHeight
                width: height
                radius: height/2
                color: root.bulletPointColor
            }

            Label{
                id: label
                anchors{
                    left: bulletPoint.right
                    right: parent.right
                    leftMargin: root.bulletPointSpacing
                }
                wrapMode: Label.Wrap
                text: modelData
            }
        }
    }
}

/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Item{
    id: root
    width: parent.contentWidth ? parent.contentWidth : parent.width
    height: type < 2 ? childrenRect.height : label.height

    property alias title: label.text

    // currently, only types 1-3 are distinguished
    property int type: 1

    Label {
        id: label
        anchors{
            left: parent.left
            right: parent.right
        }
        wrapMode: Label.Wrap
        font.bold: root.type < 3
        font.italic: root.type > 2
    }

    Rectangle{
        id: bottomLine
        anchors{
            top: label.bottom
            topMargin: units.gu(0.5)
        }
        height: units.gu(0.25)
        width: root.width
        color: label.color
        visible: root.type < 2
    }
}

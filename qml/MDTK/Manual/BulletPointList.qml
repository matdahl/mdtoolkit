/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Column {
    id: root
    x: indent
    width: (parent.contentWidth ? parent.contentWidth : parent.width) - x

    property int   itemPadding: units.gu(0.5)
    property int   indent: units.gu(2)
    property int   bulletPointPadding: units.gu(1)
    property int   bulletPointSize: units.gu(1)
    property color bulletPointColor: theme.palette.normal.base

    property alias model: repeater.model

    Repeater{
        id: repeater
        model: []
        delegate: Item{
            width: root.width
            height: lbText.height + 2*root.itemPadding

            Rectangle{
                id: bulletPoint
                y: root.itemPadding + (units.gu(2)-root.bulletPointSize)/2
                height: root.bulletPointSize
                width: root.bulletPointSize
                radius: root.bulletPointSize/2
                color: root.bulletPointColor
            }

            Label{
                id: lbText
                anchors{
                    left: bulletPoint.right
                    right: parent.right
                    leftMargin: root.bulletPointPadding
                }
                y: root.itemPadding
                wrapMode: Label.Wrap
                text: modelData
            }
        }
    }
}

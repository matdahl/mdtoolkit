/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Qt.labs.settings 1.1
import Ubuntu.Components.Popups 1.3

Item {
    id: root
    anchors.fill: parent

    Settings{
        id: manualSettings
        category: "Manual"
        property bool showOnStart: true
    }

    Component.onCompleted: {
        if (manualSettings.showOnStart)
            open()
    }

    property string dialogComponent

    function open(){
        PopupUtils.open(Qt.resolvedUrl(dialogComponent),root,{"settings" : manualSettings})
    }
}

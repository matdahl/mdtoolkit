/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mdtoolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Window 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
    id: root
    title: "Manual"

    property alias tableOfContentsVisible: tableOfContents.visible
    property alias tableOfContentsDepth:   tableOfContents.depth
    property alias tableOfContentsIndent:  tableOfContents.indent

    property alias introduction: lbIntro.text
    default property alias contents: contentColumn.data

    // to be set when open dialog
    property var settings

    Flickable{
        id: contentFlickable
        width: root.contentWidth
        height: Math.max(units.gu(20),Math.min(contentHeight,maxHeight))
        contentHeight: flickableColumn.height
        clip: true

        readonly property int maxHeight: Window.height - footer.height - x - units.gu(20)

        Column{
            id: flickableColumn
            width: contentFlickable.width
            spacing: units.gu(2)

            ManualParagraph{
                id: lbIntro
            }

            Column{
                id: tableOfContents
                width: flickableColumn.width
                spacing: units.gu(1)

                property int depth: 2
                property int indent: units.gu(2)

                ManualSeparator{}

                Repeater{
                    model: contentColumn.children
                    delegate: MouseArea{
                        width: tableOfContents.width
                        height: tocLabel.height
                        readonly property bool isHeader: modelData instanceof ManualHeader
                        visible: isHeader && (modelData.type <= tableOfContents.depth)

                        Label{
                            id: tocLabel
                            anchors{
                                left: parent.left
                                right: parent.right
                                leftMargin: (modelData.type-1) * tableOfContents.indent
                            }
                            wrapMode: Label.Wrap
                            text: modelData.title ? modelData.title : ""
                            font.bold: modelData.type < 2
                            font.italic: modelData.type > 2
                            color: theme.palette.normal.activity
                        }

                        onClicked: contentFlickable.contentY = contentColumn.y + modelData.y
                    }
                }

                ManualSeparator{}
            }

            Column{
                id: contentColumn
                width: flickableColumn.width
                spacing: units.gu(2)
            }
        }
    }

    ManualFooter{
        id: footer
        showOnStart: root.settings.showOnStart
        onShowOnStartChanged: root.settings.showOnStart = showOnStart
        onClosed: PopupUtils.close(root)
    }
}

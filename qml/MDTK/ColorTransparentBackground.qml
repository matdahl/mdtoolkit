/*
 * Copyright (C) 2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12

Item{
    id: root

    property alias radius:   shader.radius
    property alias gridSize: shader.gridSize
    property alias color1:   shader.color1
    property alias color2:   shader.color2

    ShaderEffect{
        id: shader
        anchors.fill: parent
        property real radius: 0.
        property real gridSize: units.gu(1)
        readonly property real guW: gridSize/width
        readonly property real guH: gridSize/height
        readonly property real r2:  radius*radius

        property color color1: theme.palette.normal.background
        property color color2: theme.palette.normal.foreground

        fragmentShader: "
            uniform mediump float width;
            uniform mediump float height;
            uniform mediump float radius;
            uniform mediump float r2;
            uniform mediump float guW;
            uniform mediump float guH;
            uniform mediump vec4  color1;
            uniform mediump vec4  color2;
            uniform mediump float qt_Opacity;
            varying highp   vec2  qt_TexCoord0;

            mediump float roundedCorners(mediump vec2 pos){
                // project position to top left corner and transform to pixel distance from curvature center
                mediump vec2 projected = min(vec2(1.0,1.0) - pos,pos) * vec2(width,height) - radius;

                // compute distance squared from curvature center
                mediump float d2 = dot(projected,projected)
                                    // position is always on when x or y > 0 -> null distance in this case
                                    * step(projected.x,0.0)
                                    * step(projected.y,0.0);

                return step(d2,r2);
            }

            void main() {
                mediump float pos = floor(qt_TexCoord0.x/guW) + floor(qt_TexCoord0.y/guH);
                gl_FragColor = ((mod(pos, 2.0) == 0.0) ? color1 : color2)
                                * roundedCorners(qt_TexCoord0)
                                * qt_Opacity;
            }"

    }
}
